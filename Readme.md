# General

This program enables writers to write their books directly into a mindmap, using a mind mapping software such as FreeMind. The only supported format is `.mm` at the moment. Additionally, this program analyzes your book and gives **statistics** on the number of words used for every chapter.

The supported output files are currently **PDF** (you specify the format, they can be uploaded to any platform including KDP and Lulu) and **EPUB** (those should be valid, they are working in any epub platform including KDP or Lulu). **RTF** is also supported in a broken way, feel free to contribute, it's not so difficult to fix it!

The advantage of working with your source file in a mindmap is that everything is in one place: the description for your characters, the topics you want to address or the outline of your story. Additionally, every chapter appears in a tree and the contents is displayed in every node. Navigating through the document and its chapters is therefore much easier than in any WYSYWYG text handling software (MS Word, Libre Office, etc.). Besides, you also have your characters or any useful information in other nodes that are reachable with a simple mouse click.

Additionally, **you can generate any number of different paperback formats** (eg. one small pocket format and a bigger format for people with poor eyesight, etc.) from the same source.

You whole mindmap can have virtually any form. Arrange it as you wish.

The only constraints is that it should absolutely have 3 main nodes under the root node of the mindmap (beware, these are case sensitive):

- `$Main$` which is the core of the text for your book, anything under that node is considered as a chapter, and subnodes are considered as subchapters.
- `$Meta$` which contains metadata about the book (see below for more details),
- `$Formats$` which contains information about the different formats in which you wish to release your book (see below for more details).

All other nodes are simply ignored by the converter, so you can use them for whatever you need.

# Technical Requirements, Usage

- FreePlane version 1.10 (I have had troubles with versions above this for now),
- Java (11 or 17 are now supported).

Simply download the jar file and execute it with the following parameters:

- .mm file,
- target format, the exact same name than in the “formats” section of your mindmap.

Example, given that mybook.mm contains a node called “pdf” under the “$Formats$” node:

```
java -jar mmbookgenerator.jar mybook.mm pdf
```

# The main book

Just add nodes under the node called `$Main$` and write notes inside the nodes, they will be considered as chapters and subchapters and the notes are the contents. Notes are in HTML which enables you to use basic formatting (including images) in your text.

Automatic numbering of chapters: just start the chapter name with a `#`. No space after it. Any chapter not starting with the `#` will not be numbered (useful for preface, etc.). Subchapter numbering is currently not supported.

# Metadata

The book must contain metadata in a special node called `$Meta$`.

This node contains the following subnodes:

- `Title`, the title of the book, one line only,
- `Author`, the name of the author(s) of the book, one line only,
- `Publisher`, the name of the publisher for this book, one line only,
- `Language`, in which language the book is written, only FR and EN are currently supported, one line only,
- `Description`, a full text description with possibly multiple paragraphs,
- `DateTZ`, the publication date for the book,  in the format 2018-09-15T00:00:00+00:00,
- `UUID`, a unique UUID for this book.

# Formats

Add any number of nodes under the `$Formats$` node. The label of these nodes must start with pdf, epub or rtf, and optionally followed by some extra description for yourself.

Inside nodes, the format is a series of properties in the form `propertyName=propertyValue`.

For Epub, only the property `ISBN` can be used for now.

For RTF, no properties are currently supported.

For PDF, the following properties are mandatory (numbers are in millimeters and are decimal numbers unless stated otherwise):

- `PageWidth`, the width of the paper page,
- `PageHeight`, the height of the paper page,
- `TopMargin`, the distance of the text from the top of the page, note that you have to leave some space for the header,
- `BottomMargin`, the distance of the text from the bottom of the page, 
- `InnerMargin`, the distance of the text from the edge of the paper on the outside of the book (eg. to the right on odd pages, to the left on even pages),
- `OuterMargin`, the distance of the text from the edge of the paper on the inside of the book,
- `NormalFontSize`, the default font size of the text in points,
- `Interlines`, how much space should be taken by a line, eg. 1.5 leaves a half line between each line in the text (in other words, each lines occupies a vertical space of 1.5 line),
- `ExtraInterPar`, this is an extra space for titles and is added before the title line,
- `TitleFontSizes`, the sizes, in points, of the title levels. These are integers separated by commas, you should have as many of these as the number of subchapter levels in the book (otherwise the conversion will crash),
- `ChapterStartOnEven`, a boolean (0 or 1, or true or false), if `true`, then the generator skips a page to make sure that all chapters start on an even page,
- `ISBN`, this is an optional property to give the ISBN of the book for this format (normally, every paperback format has its own ISBN).

Note that you can use the tag `$ISBN$` anywhere inside the book. Whenever this tag is in a paragraph, then:

- if an ISBN is specified in the current format, then the `$ISBN$` tag is substituted with that ISBN number,
- if no ISBN is specified in the current format, the whole paragraph is deleted. This is useful when you want to show an ISBN number at the beginning or at the end of your book, but you want to ignore that line for formats that don't have an ISBN.

You can also specify the following additional properties:

- `NotesFontSize`, the font size of all notes (default is 8),
- `NotesNoteSpacing`, the vertical spacing between notes (default is 1.7),
- `NotesInterval`, the vertical spacing between lines inside notes (default is 0.9),
- `NotesDistanceFromText`, the vertical spacing between normal text and the notes at the bottom of a page (default is 0.8),
- `FontNameMain`, the name of the font used for normal plain text (default is “Liberation Serif”),
- `FontNameTitles`, the name of the font used for titles (default is “Helvetica”),
- `FontNameCaptions`, the name of the font used for captions (default is “Helvetica”),
- `FontNameCitations`, the name of the font used for quotes (citations) (default is “Helvetica”),
- `FontNameNotes`, the name of the font used for notes (default is “Helvetica”),
- `FontNameHeaders`, the name of the font used for headers on every page (book name, chapter names…) (default is the same as the main font for normal text),
- `HeadersFontSize`, the font size for headers (default is the same as normal main font size for text).

The program is packaged with the following fonts by default:

- Acari,
- Helvetica,
- LiberationSerif,
- NishikiTeki.

You can provide additional ttf fonts in the classpath of the app, under a folder called “ttf”. To use those fonts, simply use their names in the above properties, the program will look for font files that are called 'fontname-Regular.ttf', 'fontname-Bold.ttf', 'fontname-Italic.ttf', 'fontname-BoldItalic.ttf'. If the Bold/Italic versions are not found, then the Regular font is used instead.

# Additional requirements

To generate an epub, you need to have a file called `cover.jpg` in the same directory than your `.mm`. This file will be used as the cover for your book.

All images linked inside the mindmap to be included in the book should be located in a subdirectory called `images` under the directory where the mindmap is located.

# Additional tags

Some extra tags are allowed at the beginning of lines:

- [vcenter] to center a number of paragraphs vertically in the page,
- [citation] to place a quote from someone else, which restricts the available margins for the text, you still need to justify the different parts (eg. justify right for the quoted person's name),
- [caption] a caption or legend, typically under an image. The legend will stick to the image on page turn,
- [cadre] is also in the works to surround some text with a frame.

# HTML tags

A limited number of HTML tags are supported:

- i, b, sup, br,
- a creates hyperlinks,
- img imports images into the document,
- ul/li transforms the list into a bullet list.

# Foot Notes

Foot notes are now supported. To create a foot note, simply add a node whose name starts with “$note$ ” (don’t forget the space between the last '$' and the name) followed by a unique note name, preferably under the node where this note will be used (but it is not mandatory). Type the note’s text contents in the node’s html text.

To reference that note in your document, simply use the following anywhere inside the text of your book:

- “{foot-note:unique-node-name}” or “{fn:unique-node-name}” to create a foot note on the same page,
- “{chap-note:unique-node-name}” or “{cn:unique-node-name}” to create a note that will be placed at the end of the current chapter,
- “{glob-note:unique-node-name}” or “{gn:unique-node-name}” to create a note at the end of the document (in a specific “Notes” section).

As epubs don’t have pages, all notes (of all types) are appended to the final “Notes” section in this format.

# Restrictions:

- the TOC is generated automatically and is located at the end of the document (only supported in PDF format), it currently contains only the chapters of the first level,
- only French and English are currently supported, adding a new language is extremely straightforward, though,
- a few html tags are currently supported: `i`, `b`, `sup`, `img` (no inline ones, they are on a separate line for now, and located in a subdirectory images - see "Additional Requirements"). Styles for justifying paragraphs are taken into account. All other tags are currently ignored,
- no numbering of graphics are supported yet,
- RTF seems to suffer from encoding problems. It may work for a pure ASCII encoded document, but will certainly fail for others. I don't really mind since I don't use it anymore. Fix it if you need it. :)

# Licence

This software is licensed under GPL3. Use it as you will and at your own risk. Feel free to contribute.
