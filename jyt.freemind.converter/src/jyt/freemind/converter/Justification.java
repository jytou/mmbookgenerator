package jyt.freemind.converter;

/**
 * Used to identify the justification of lines.
 * @author jytou
 */
public enum Justification
{
	JUSTIFIED,
	LEFT,
	CENTER,
	RIGHT;
}
