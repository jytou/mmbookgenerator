package jyt.freemind.converter;

import java.util.HashMap;
import java.util.Map;

/**
 * A FreeMind document, including metadata.
 */
public class FMDocument
{
	// The resulting root node
	private FMNode mRootNode = null;
	// All metadata
	private Map<String, String> mMetas = new HashMap<>();
	// All formats
	private Map<String, Map<String, String>> mFormats = new HashMap<>();
	// All existing notes: global, chapter and foot notes
	private Map<String, FMNote> mNotes = new HashMap<>();

	public void setRootNode(FMNode pRootNode)
	{
		mRootNode = pRootNode;
	}

	public FMNode getRootNode()
	{
		return mRootNode;
	}

	public Map<String, String> getMetas()
	{
		return mMetas;
	}

	public Map<String, Map<String, String>> getFormats()
	{
		return mFormats;
	}

	public Map<String, FMNote> getNotes()
	{
		return mNotes;
	}
}
