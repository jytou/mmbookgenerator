package jyt.freemind.converter;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.text.StringEscapeUtils;

import jyt.freemind.converter.epub.EpubBuilder;
import jyt.freemind.converter.pdf.PDFBuilder;
import jyt.freemind.converter.rtf.RTFBuilder;

/**
 * Converts a mindmap into a readable document.
 * @author jytou
 */
public class ConvertMM
{
	private static final String BEFORE_NODE_TEXT = " TEXT=\"";
	public static Properties sLangProps;

	/**
	 * Parameters are:
	 * - the path to the mindmap,
	 * - the format to be used to generate the document, as labeled inside the mindmap under "$Formats$".
	 * Note that if the format is omitted, the program analyzes the document and prints statistics on words only.
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception
	{
		// Parse the mindmap and extract the nodes
		String original = args[0];
		final SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
		final FMDocument document = new FMDocument();
		final SAXHandler handler = new SAXHandler(document);
		String contents = new String(Files.readAllBytes(Paths.get(original)));
		contents = contents.replaceAll("</sup>&nbsp;", "</sup> ");
		contents = contents.replaceAll("&nbsp;", "&#160;");
//		contents = contents.replaceAll("&nbsp;", "&#160;").replaceAll(" CONTENT-TYPE=\"xml/\"", "");
//		contents = contents.replaceAll(" CONTENT-TYPE=\"xml/\"", "");
//		System.out.println(contents);
		// find any nodes that may have html entities in them and replace them with their UTF equivalent
		StringBuilder sb = new StringBuilder();
		int prev = 0;
		int pos = 0;
		while ((pos = contents.indexOf(BEFORE_NODE_TEXT, prev)) > 0)
		{
			pos += BEFORE_NODE_TEXT.length();
			sb.append(contents.substring(prev, pos));
			prev = pos;
			pos = contents.indexOf('"', pos);
			final String nodeTitle = contents.substring(prev, pos);
//			System.out.println(nodeTitle);
			final String unescapedNodeTitle = StringEscapeUtils.escapeJava(StringEscapeUtils.unescapeHtml4(nodeTitle).replaceAll("&apos;", "'")).replaceAll("\\\\u([0-9A-F]{4})", "\\&\\#$1;");
			if (!unescapedNodeTitle.equals(nodeTitle))
				System.out.println(nodeTitle + " => " + unescapedNodeTitle);
//			sb.append(unescapedNodeTitle);
			sb.append(nodeTitle);
			sb.append("\"");
			prev = pos + 1;
		}
		sb.append(contents.substring(prev));
		contents = sb.toString();
//		Files.write(Paths.get(original + ".xml"), contents.getBytes());

//		System.out.println(contents);
		parser.parse(new BOMInputStream(new ByteArrayInputStream(contents.getBytes())), handler);

		// Compute the node sizes
		final FMNode rootNode = document.getRootNode();
		rootNode.computeSize();

		// Print the metadata of the book
		final Map<String, String> meta = document.getMetas();
		ConvUtil.printMeta(meta);
		final InputStream langPropsAsStream = PDFBuilder.class.getResourceAsStream("/lang-" + meta.get("Language") + ".properties");
		if (langPropsAsStream == null)
			throw new RuntimeException("Language not supported: " + meta.get("Language"));
		sLangProps = new Properties();
		sLangProps.load(langPropsAsStream);

		// Extract the formats and show them
		final Map<String, Map<String, String>> formats = document.getFormats();
		ConvUtil.printFormats(formats);

		// Print the whole tree with statistics
		ConvUtil.printNodes(rootNode, 0);

		if (args.length > 1)
		{
			final File parentFile = new File(new File(original).getAbsolutePath()).getParentFile();
			// Grab the required format
			for (int i = 1; i < args.length; i++)
			{
				final String formatName = args[i];
				System.out.println("Generating " + formatName);
				final FMNode node = rootNode.cloneTree();
				if (formatName.startsWith("rtf"))
					RTFBuilder.buildRTFFile(node, meta, ConvUtil.createFileName(original, formatName, "rtf"));
				else
				{
					final Map<String, String> format = formats.get(formatName);
					if (format == null)
					{
						System.err.println("Format not found in mind map: " + args[1]);
						System.exit(1);
					}

					// Load the ISBN into the metadata (that's for historical reasons, the format may not be available when we need it, but the metadata will be)
					if (format.containsKey(ConvUtil.KEY_ISBN) && (!format.get(ConvUtil.KEY_ISBN).isEmpty()))
						document.getMetas().put(ConvUtil.KEY_ISBN, format.get(ConvUtil.KEY_ISBN));
					// Depending on the format, call the corresponding builder
					if (formatName.startsWith("epub"))
						EpubBuilder.buildEpubFile(document, parentFile, node, meta, ConvUtil.createFileName(original, formatName, "epub"));
					else if (formatName.startsWith("pdf"))
					{
						PDFBuilder.buildPDFFile(document, parentFile, node, meta, format, ConvUtil.createFileName(original, formatName, "pdf"));
						System.out.println("\n*************** WITH PAGE NUMBERS ************\n");
						// Print the whole tree with statistics and page numbers
						ConvUtil.printNodes(node, 0);
					}
					else
						throw new RuntimeException("Unknown format for " + formatName);
				}
			}
		}
	}
}
