package jyt.freemind.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Some general utilities.
 */
public class ConvUtil
{
	// The key used to store the ISBN in the metadata
	public static final String KEY_ISBN = "ISBN";
	// The tag used in documents to replace with the ISBN
	public static final String ISBN_TAG = "$ISBN$";
	// Delimiters used to isolate words.
	public static final String DELIMS = " \t\n";
	// Current chapter number to show them in the console.
	private static int sChapNumConsole = 1;

	/**
	 * Returns a property as a string, throws an exception if the property was not found
	 * @param pFormat
	 * @param pPropName
	 * @return
	 */
	public static String getStringProp(Map<String, String> pFormat, String pPropName, String pDefaultValue)
	{
		if (!pFormat.containsKey(pPropName))
			if (pDefaultValue == null)
				throw new RuntimeException("Property " + pPropName + " not found in file for current format");
			else
				return pDefaultValue;
		return pFormat.get(pPropName);
	}

	/**
	 * Returns the boolean value of a property (<code>true</code> if 1 or 'true', false otherwise).
	 * @param pFormat
	 * @param pPropName
	 * @return
	 */
	public static boolean getBooleanProp(Map<String, String> pFormat, String pPropName)
	{
		String value = getStringProp(pFormat, pPropName, null);
		return "1".equals(value) || "true".equals(value.toLowerCase());
	}

	/**
	 * Returns an array of integer values of a property (separated by commas).
	 * @param pFormat
	 * @param pPropName
	 * @return
	 */
	public static int[] getIntArrayProp(Map<String, String> pFormat, String pPropName)
	{
		String value = getStringProp(pFormat, pPropName, null);
		List<Integer> values = new ArrayList<>();
		StringTokenizer st = new StringTokenizer(value, ",");
		while (st.hasMoreTokens())
			values.add(Integer.parseInt(st.nextToken().trim()));
		int[] array = new int[values.size()];
		for (int i = 0; i < array.length; i++)
			array[i] = values.get(i);
		return array;
	}

	/**
	 * Returns the {@link Float} value of a property.
	 * @param pFormat
	 * @param pPropName
	 * @return
	 */
	public static float getFloatProp(Map<String, String> pFormat, String pPropName)
	{
		return Float.parseFloat(getStringProp(pFormat, pPropName, null));
	}

	/**
	 * Returns the {@link Float} value of a property.
	 * @param pFormat
	 * @param pPropName
	 * @param pDefault
	 * @return
	 */
	public static float getFloatProp(Map<String, String> pFormat, String pPropName, Float pDefault)
	{
		String stringProp = null;
		try
		{
			stringProp = getStringProp(pFormat, pPropName, null);
		}
		catch (RuntimeException e)
		{
		}
		if (stringProp != null)
			return Float.parseFloat(stringProp);
		else
			return pDefault;
	}

	/**
	 * Returns the {@link Float} value of a property.
	 * @param pFormat
	 * @param pPropName
	 * @return
	 */
	public static int getIntProp(Map<String, String> pFormat, String pPropName, Integer pDefault)
	{
		String stringProp = null;
		try
		{
			stringProp = getStringProp(pFormat, pPropName, null);
		}
		catch (RuntimeException e)
		{
		}
		if (stringProp != null)
			return Integer.parseInt(stringProp);
		else
			return pDefault;
	}

	/**
	 * Prints all the formats to the console.
	 * @param pFormats
	 */
	static void printFormats(Map<String, Map<String, String>> pFormats)
	{
		for (String name : pFormats.keySet())
			System.out.println(name + ": " + pFormats.get(name));
	}

	/**
	 * Prints all nodes and their statistics.
	 * @param pNode
	 * @param pLevel
	 */
	static void printNodes(FMNode pNode, int pLevel)
	{
		String label = pNode.getLabel();
		if (label.startsWith("#"))
			label = String.valueOf(sChapNumConsole++) + " " + label.substring(1);
		System.out.println(spaces(pLevel) + label + " (" + pNode.getWords() + (pNode.getStartPage() == -1 ? "" : " - p" + pNode.getStartPage()) + ")");
		/*if (pNode.getParagraphs() != null)
			for (String paragraph : pNode.getParagraphs())
				System.out.println(paragraph);*/
		for (FMNode node : pNode.getChildren())
			printNodes(node, pLevel + 1);
	}

	/**
	 * Prints metadata to the console.
	 * @param pMetas
	 */
	static void printMeta(Map<String, String> pMetas)
	{
		System.out.println(pMetas);
	}

	/**
	 * Returns <code>pLevel</code> spaces.
	 * @param pLevel
	 * @return
	 */
	private static String spaces(int pLevel)
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < pLevel; i++)
			sb.append(' ');
		return sb.toString();
	}

	/**
	 * Returns the level of the node in its hierarchy.
	 * @param pNode
	 * @return
	 */
	public static int getNodeLevel(FMNode pNode)
	{
		if (pNode.getParent() == null)
			return 0;
		else
			return getNodeLevel(pNode.getParent()) + 1;
	}

	/**
	 * Generates a file name from the original file name and format+extension.
	 * @param pOriginal
	 * @param pFormatName
	 * @param pExtension
	 * @return
	 */
	public static String createFileName(String pOriginal, final String pFormatName, String pExtension)
	{
		final String rtfFile = pOriginal.substring(0, pOriginal.lastIndexOf('.')) + "-" + pFormatName + "." + pExtension;
		return rtfFile;
	}
}
