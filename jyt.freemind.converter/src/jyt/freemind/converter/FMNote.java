package jyt.freemind.converter;

import java.util.List;

/**
 * Represents a note - with an id and a corresponding text
 */
public class FMNote
{
	private String mId;
	private List<FMParagraph> mContents;

	public FMNote(String pId, List<FMParagraph> pContents)
	{
		super();
		mId = pId;
		mContents = pContents;
	}

	public String getId()
	{
		return mId;
	}

	public List<FMParagraph> getContents()
	{
		return mContents;
	}
}
