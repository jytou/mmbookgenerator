package jyt.freemind.converter.pdf;

import java.util.ArrayList;
import java.util.List;

import jyt.freemind.converter.NoteReference;

public class PDFNotesContext
{
	private List<NoteReference> mFootNotes = new ArrayList<>();
	private List<NoteReference> mChapNotes = new ArrayList<>();
	private List<NoteReference> mGlobNotes = new ArrayList<>();
	private float mCurrentPageBottomSpace;

	public PDFNotesContext(float pCurrentPageBottomSpace)
	{
		super();
		mCurrentPageBottomSpace = pCurrentPageBottomSpace;
	}

	public List<NoteReference> getFootNotes()
	{
		return mFootNotes;
	}

	public List<NoteReference> getChapNotes()
	{
		return mChapNotes;
	}

	public List<NoteReference> getGlobNotes()
	{
		return mGlobNotes;
	}

	public float getCurrentPageBottomSpace()
	{
		return mCurrentPageBottomSpace;
	}

	public void setCurrentPageBottomSpace(float pSpace)
	{
		mCurrentPageBottomSpace = pSpace;
	}

	public void addCurrentPageBottomSpace(float pExtraSpace)
	{
		mCurrentPageBottomSpace += pExtraSpace;
	}
}
