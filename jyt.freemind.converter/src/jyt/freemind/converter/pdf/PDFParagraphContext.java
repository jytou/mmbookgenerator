package jyt.freemind.converter.pdf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.StringTokenizer;

import org.apache.pdfbox.pdmodel.font.PDFont;

import jyt.freemind.converter.ConvUtil;
import jyt.freemind.converter.FMParagraph;
import jyt.freemind.converter.FMParagraph.Property;
import jyt.freemind.converter.Justification;

public class PDFParagraphContext
{
	// Current being addressed
	private FMParagraph mParagraph;
	// The text of the paragraph, note that it may be different than the text in the paragraph due to preprocessing
	private String mTxt;
	// current position in the token list
	private int mCurPos = 0;
	// Tokens detected in the paragraph's text
	private List<String> mTokenList;
	// Default justification for this entire paragraph
	private Justification mJustification = Justification.JUSTIFIED;
	// is this paragraph a quote?
	private boolean mCitation = false;
	// is this paragraph a block of text that needs to be glued together?
	private boolean mCadre = false;
	// True if this is a paragraph from a bullet list
	private boolean mList = false;
	// Indentation for whole paragraph (used typically for notes or lists)
	private float mParIndent = 0f;
	// all current modifiers in the paragraph (italic, bold, etc.)
	private Stack<PDFFontModifier> mModifiersStack = new Stack<>();
	private Set<PDFFontModifier> mModifiersAsSet = new HashSet<>();
	// the current font name
	private String mFontName;
	// the current font size
	private float mFontSize;
	// the cache for the current font
	private PDFont mCurrentFont = null;
	// how many points does a space take?
	private float mSpaceWidth = 0;

	public PDFParagraphContext(FMParagraph pParagraph, final String pFontName, final float pFontSize) throws IOException
	{
		super();
		mParagraph = pParagraph;
		mTxt = mParagraph.getText();
		mFontName = pFontName;
		mFontSize = pFontSize;

		// First check the paragraph properties for anything special
		for (Property prop : pParagraph.getProperties())
		{
			if (Property.JUSTIFICATION.equals(prop))
			{
				final Object align = pParagraph.getPropertyValue(prop);
				if ("center".equals(align.toString().toLowerCase()))
					setJustification(Justification.CENTER);
				else if ("right".equals(align.toString().toLowerCase()))
					setJustification(Justification.RIGHT);
			}
			else if (Property.CITATION.equals(prop))
				setCitation(true);
			else if (Property.CADRE.equals(prop))
				setCadre(true);
			else if (Property.PAR_INDENT.equals(prop))
				setParIndent((float)pParagraph.getPropertyValue(Property.PAR_INDENT));
			else if (Property.LIST.equals(prop))
				setList(true);
			else if (!(Property.VJUST.equals(prop) || Property.FIRST_INDENT.equals(prop) || Property.CAPTION.equals(prop)))
				System.err.println("Unknown property: " + prop);
		}

		initTxt();
	}

	private void initTxt()
	{
		mTokenList = new ArrayList<>();
		int pos = 0;
		do
		// Iterate on the text and build the tokens
		{
			// Move from one tag to another, if there are any
			final int infPos = mTxt.indexOf('<', pos);
			if ((infPos == -1) || (infPos >= pos))
			// There is still some potential text to be covered
			{
				if ((infPos == -1) || (infPos > pos))
				// definitely some text here
				{
					final String currentText = infPos == -1 ? mTxt.substring(pos) : mTxt.substring(pos, infPos);
					// Tokenize it with delimiters - but also grab the delimiters to know where they are and if there are any
					final StringTokenizer stBlank = new StringTokenizer(currentText, ConvUtil.DELIMS, true);
					while (stBlank.hasMoreTokens())
					{
						String s = stBlank.nextToken();//.replace((char)0xa0, ' ').replace((char)0x2212, '-');
						mTokenList.add(s);
					}
				}
				pos = infPos;
				if (pos >= 0)
				// Some more text to process with a tag
				{
					final int supPos = mTxt.indexOf('>', pos);// position of >
					if (supPos >= 0)
					// There is the closing of the tag
					{
						final String tagFull = mTxt.substring(pos, supPos + 1);
						pos = supPos + 1;
						mTokenList.add(tagFull);
					}
					else
						throw new RuntimeException("Could not find the end tag in the following paragraph: " + mTxt);
				}
			}
			else
				pos = -1;
		}
		while (pos >= 0);
	}

	public PDFParagraphContext cloneParagraph() throws IOException
	{
		final PDFParagraphContext pc = new PDFParagraphContext(mParagraph, mFontName, mFontSize);
		pc.mTxt = mTxt;
		pc.mCurPos = mCurPos;
		pc.mTokenList.addAll(mTokenList);
		pc.mJustification = mJustification;
		pc.mCitation = mCitation;
		pc.mCadre = mCadre;
		pc.mList = mList;
		pc.mParIndent = mParIndent;
		pc.mModifiersAsSet.addAll(mModifiersAsSet);
		pc.mCurrentFont = mCurrentFont;
		pc.mSpaceWidth = mSpaceWidth;
		return pc;
	}

	public int getCurPos()
	{
		return mCurPos;
	}

	public void setCurPos(int pCurPos)
	{
		mCurPos = pCurPos;
	}

	public void incCurPos()
	{
		mCurPos++;
	}

	public String getTxt()
	{
		return mTxt;
	}

	public void setTxt(String pTxt)
	{
		mTxt = pTxt;
		initTxt();
	}

	public Justification getJustification()
	{
		return mJustification;
	}
	public void setJustification(Justification pJustification)
	{
		mJustification = pJustification;
	}
	public boolean isCitation()
	{
		return mCitation;
	}
	public void setCitation(boolean pCitation)
	{
		mCitation = pCitation;
	}
	public boolean isCadre()
	{
		return mCadre;
	}
	public void setCadre(boolean pCadre)
	{
		mCadre = pCadre;
	}
	public boolean isList()
	{
		return mList;
	}
	public void setList(boolean pList)
	{
		mList = pList;
	}
	public float getParIndent()
	{
		return mParIndent;
	}
	public void setParIndent(float pParIndent)
	{
		mParIndent = pParIndent;
	}
	public String getFontName()
	{
		return mFontName;
	}
	public void setFontName(String pFontName) throws IOException
	{
		mFontName = pFontName;
		autoComputeSpaceWidth();
	}
	public float getFontSize()
	{
		return mFontSize;
	}
	public void setFontSize(float pFontSize) throws IOException
	{
		mFontSize = pFontSize;
		autoComputeSpaceWidth();
	}
	public PDFont getCurrentFont()
	{
		return mCurrentFont;
	}
	public void setCurrentFont(PDFont pCurrentFont) throws IOException
	{
		mCurrentFont = pCurrentFont;
		autoComputeSpaceWidth();
	}
	public float getSpaceWidth()
	{
		return mSpaceWidth;
	}
	public void setSpaceWidth(float pSpaceWidth)
	{
		mSpaceWidth = pSpaceWidth;
	}
	public void autoComputeSpaceWidth() throws IOException
	{
//		mSpaceWidth = Math.min(mFontSize / 5f, mCurrentFont.getSpaceWidth() / 2f / 1000f * PDFFontFactory.getSize(mFontSize, mModifiersAsSet));
//		mSpaceWidth = mCurrentFont.getSpaceWidth() / 2f / 1000f * PDFFontFactory.getSize(mFontSize, mModifiersAsSet);
		mSpaceWidth = mCurrentFont.getStringWidth("a") * 2 / 3 / 1000f * PDFFontFactory.getSize(mFontSize, mModifiersAsSet);
	}
	public FMParagraph getParagraph()
	{
		return mParagraph;
	}

	public List<String> getTokenList()
	{
		return mTokenList;
	}

	public String curToken()
	{
		if ((mCurPos >= 0) && (mCurPos < mTokenList.size()))
			return mTokenList.get(mCurPos);
		else
			return null;
	}

	public Set<PDFFontModifier> getModifiersAsSet()
	{
		return mModifiersAsSet;
	}

	public void pushModifier(PDFFontModifier pModifier) throws IOException
	{
		mModifiersStack.push(pModifier);
		mModifiersAsSet.add(pModifier);
		autoComputeSpaceWidth();
	}

	public PDFFontModifier peekModifier()
	{
		if (mModifiersStack.isEmpty())
			return null;
		return mModifiersStack.peek();
	}

	public PDFFontModifier popModifier() throws IOException
	{
		final PDFFontModifier head = mModifiersStack.pop();
		mModifiersAsSet.clear();
		mModifiersAsSet.addAll(mModifiersStack);
		autoComputeSpaceWidth();
		return head;
	}
}
