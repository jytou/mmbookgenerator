package jyt.freemind.converter.pdf;

/**
 * Different font modifiers
 * @author jytou
 */
public enum PDFFontModifier
{
	BOLD,
	ITALIC,
	SUPERSCRIPT,
	LINK,
}
