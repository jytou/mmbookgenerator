package jyt.freemind.converter.pdf;

import java.util.List;

import jyt.freemind.converter.NoteReference;

/**
 * A word in a PDF document and its various properties. Note that it can contain non breaking spaces.
 * @author jytou
 */
public class PDFWord
{
	public enum ImageType {NONE, FULL_WIDTH, INLINE};
	private String mFontName;
	private boolean mBold;
	private boolean mOblique;
	private boolean mSuperscript;
	private float mPoints;// vertical size
	private float mWidth;// horizontal width in current font and properties
	private float mNotesWidth;// horizontal width of note references
	private ImageType mImageType = ImageType.NONE;
	private String mText;
	private boolean mSpaceAfterWord = false;
	private List<NoteReference> mNoteReferences;
	private boolean mFirstWordOfParagraph = false;

	public PDFWord(String pFontName, boolean pBold, boolean pOblique, boolean pSuperscript, float pWidth, float pNotesWidth, float pPoints, String pText, List<NoteReference> pNoteReferences, boolean pFirstWordOfParagraph)
	{
		super();
		mFontName = pFontName;
		mBold = pBold;
		mOblique = pOblique;
		mSuperscript = pSuperscript;
		mWidth = pWidth;
		mNotesWidth = pNotesWidth;
		mPoints = pPoints;
		mText = pText;
		mNoteReferences = pNoteReferences;
		mFirstWordOfParagraph = pFirstWordOfParagraph;
	}

	public PDFWord(String pFontName, float pWidth, float pNotesWidth, float pPoints, String pText, List<NoteReference> pNoteReferences, boolean pFirstWordOfParagraph)
	{
		this(pFontName, false, false, false, pWidth, pNotesWidth, pPoints, pText, pNoteReferences, pFirstWordOfParagraph);
	}

	/**
	 * This is exclusively for images
	 * @param pWidth
	 * @param pHeight
	 * @param pImageSrc
	 */
	public PDFWord(float pWidth, float pHeight, String pImageSrc, ImageType pImageType, boolean pFirstWordOfParagraph)
	{
		super();
		mWidth = pWidth;
		mPoints = pHeight;
		mText = pImageSrc;
		mImageType = pImageType;
		mFirstWordOfParagraph = pFirstWordOfParagraph;
	}

	public String getFontName()
	{
		return mFontName;
	}

	public boolean isBold()
	{
		return mBold;
	}

	public boolean isOblique()
	{
		return mOblique;
	}

	public boolean isSuperscript()
	{
		return mSuperscript;
	}

	public float getWidth()
	{
		return mWidth;
	}

	public float getNotesWidth()
	{
		return mNotesWidth;
	}

	public float getPoints()
	{
		return mPoints;
	}

	public ImageType getImageType()
	{
		return mImageType;
	}

	public String getText()
	{
		return mText;
	}

	public void setSpaceAfterWord(boolean pSpaceAfterWord)
	{
		mSpaceAfterWord = pSpaceAfterWord;
	}

	public boolean hasSpaceAfterWord()
	{
		return mSpaceAfterWord;
	}

	public List<NoteReference> getNoteReferences()
	{
		return mNoteReferences;
	}

	public boolean isFirstWordOfParagraph()
	{
		return mFirstWordOfParagraph;
	}
}
