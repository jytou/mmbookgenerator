package jyt.freemind.converter.pdf;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;

/**
 * Helps retrieving the correct PDF fonts and cache them for faster processing.
 * @author jytou
 */
public class PDFFontFactory
{
	private static PDDocument mCurrentDocument = null;// this is because we want to clear the cache when we change documents
	final private static Map<String, PDFont> mCache = new HashMap<>();

	public static PDFont getFont(final PDDocument pDoc, PDFWord pWord) throws IOException
	{
		return getFont(pDoc, pWord.getFontName(), pWord.isBold(), pWord.isOblique());
	}

	public static PDFont getFont(final PDDocument pDoc, final String pFontName, Set<PDFFontModifier> pModifiers) throws IOException
	{
		if (pFontName == null)
			throw new IOException("Font name cannot be null");
		return getFont(pDoc, pFontName, pModifiers.contains(PDFFontModifier.BOLD), pModifiers.contains(PDFFontModifier.ITALIC));
	}

	public static PDFont getFont(final PDDocument pDoc, final String pFontName, final boolean pBold, final boolean pOblique) throws IOException
	{
		if (pFontName == null)
			throw new IOException("Font name cannot be null");
		String modif = "Regular";
		if (pBold)
			if (pOblique)
				modif = "BoldItalic";
			else
				modif = "Bold";
		else if (pOblique)
			modif = "Italic";
		try
		{
			return getFromCache(pDoc, pFontName + "-" + modif);
		}
		catch (IOException e)
		{
			if (!"Regular".equals(modif))
			{
				System.err.println("!! WARNING !! " + pFontName + "-" + modif + " is missing");
				return getFromCache(pDoc, pFontName + "-Regular");
			}
			else
				throw e;
		}
	}

	private static synchronized PDFont getFromCache(final PDDocument pDoc, final String pFileName) throws IOException
	{
		if (pDoc != mCurrentDocument)
		// Reset cache when we switch to another document - we have to include the correct fonts in the new one
		{
			mCurrentDocument = pDoc;
			mCache.clear();
		}
		PDFont pdFont = mCache.get(pFileName);
		if (pdFont == null)
		{
			InputStream ttfAsStream = PDFFontFactory.class.getResourceAsStream("/jyt/freemind/converter/ttf/" + pFileName + ".ttf");
			if (ttfAsStream == null)
				ttfAsStream = PDFFontFactory.class.getResourceAsStream("/jyt/freemind/converter/ttf/" + pFileName + ".otf");
			if (ttfAsStream == null)
				throw new IOException("Font not found: " + pFileName);
			pdFont = PDType0Font.load(pDoc, ttfAsStream, true);
			mCache.put(pFileName, pdFont);
		}
		return pdFont;
	}

	public static float getSize(final float pPoints, Set<PDFFontModifier> pModifiers)
	{
		return getSize(pPoints, pModifiers.contains(PDFFontModifier.SUPERSCRIPT));
	}

	public static float getSize(PDFWord pWord)
	{
		return getSize(pWord.getPoints(), pWord.isSuperscript());
	}

	public static float getSize(final float pPoints, final boolean pSuperscript)
	{
		return pSuperscript ? pPoints / 2 : pPoints;
	}
}
