package jyt.freemind.converter.pdf;

import java.util.HashSet;
import java.util.Set;

import jyt.freemind.converter.FMParagraph;
import jyt.freemind.converter.Justification;

/**
 * A line in a PDF file. Contains a series of words, the total line width, the justification, etc.
 * @author jytou
 */
public class PDFLine
{
	public enum PDFLineProperty
	{
		FIRST_INDENT,
		IMAGE,
		CITATION,
		CAPTION,
		CADRE,
		FIRST_OF_LIST,
		TITLE1,
		TITLE2,
		TITLE3,
		TITLE4,
		TITLE5,
	};

	// list of words composing this line
	private PDFWord[] mLine;
	private Justification mJustification = Justification.JUSTIFIED;
	private boolean mCitation;
	private boolean mCadre;
	private float mSpaceAfter = 0;
	private float mSpaceBefore = 0;
	private float mIndent = 0;
	private Set<PDFLineProperty> mGluedWith = null;// if non null, will attach this line to the next if the next is of that type or these types - if empty then all lines
	private float mYPos = -1;// Force the Y position on the context before printing this line
	private Set<PDFLineProperty> mLineProperties = new HashSet<>();

	public PDFLine(PDFWord[] pLine, boolean pCitation, boolean pCadre)
	{
		super();
		mLine = pLine;
		mCitation = pCitation;
		mCadre = pCadre;
	}

	public PDFWord[] getWords()
	{
		return mLine;
	}

	public float getYPos()
	{
		return mYPos;
	}

	public void setYPos(float pYPos)
	{
		mYPos = pYPos;
	}

	public Set<PDFLineProperty> getGluedWith()
	{
		return mGluedWith;
	}

	public void addGluedWith(PDFLineProperty pGluedWith)
	{
		if (mGluedWith == null)
			mGluedWith = new HashSet<>();
		mGluedWith.add(pGluedWith);
	}

	public void clearGluedWith()
	{
		mGluedWith = null;
	}

	public Justification getJustification()
	{
		return mJustification;
	}

	public void setJustification(Justification pJustification)
	{
		mJustification = pJustification;
	}

	public float getIndent()
	{
		return mIndent;
	}

	public void setIndent(float pIndent)
	{
		mIndent = pIndent;
	}

	public boolean isCitation()
	{
		return mCitation;
	}

	public boolean isCadre()
	{
		return mCadre;
	}

	public float getSpaceAfter()
	{
		return mSpaceAfter;
	}

	public void setSpaceAfter(float pSpaceAfter)
	{
		mSpaceAfter = pSpaceAfter;
	}

	public float getSpaceBefore()
	{
		return mSpaceBefore;
	}

	public void setSpaceBefore(float pSpaceBefore)
	{
		mSpaceBefore = pSpaceBefore;
	}

	public Set<PDFLineProperty> getLineProperties()
	{
		return mLineProperties;
	}

	public void addLineProperty(PDFLineProperty pLineProperty)
	{
		mLineProperties.add(pLineProperty);
	}
}
