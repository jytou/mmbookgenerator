package jyt.freemind.converter.pdf;

import java.io.File;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;

import jyt.freemind.converter.FMDocument;
import jyt.freemind.converter.NoteReference;

/**
 * The context of the document while generating it.<br>
 * Contains main information on the PDF document, current page, current position in the page, page number, and page numbers for chapters.
 * @author jytou
 */
public class PDFDocumentContext
{
	private FMDocument mFMDocument;
	// Source directory from which to read (images, for instance)
	private File mSourceDir;
	// Main PDF document
	private PDDocument mDocument;
	// Current page
	private PDPage mCurrentPage;
	// Stream of the current page
	private PDPageContentStream mContent;
	// Y position in the page
	private float mYPos;
	// Different chapter numbers - a stack
	private Stack<MutableInt> mChapterNumbers = new Stack<>();
	// Current chapter's title (for page headers)
	private String mCurChapTitle = null;
	// Current page number
	private int mCurPageNum = 1;
	// Title for the book - to be printed on the header of even pages
	private String mBookTitle = null;
	// For the TOC
	private List<Map.Entry<String, Integer>> mChapterPages = new ArrayList<>();
	// In case we want to stop the generation
	private boolean mStopGeneration = false;
	// notes to display on the current page. Every array is one note
	private List<NoteReference> mNotesOnPage = new ArrayList<>();
	// height of the notes to be printed on this page
	private float mNotesHeight = 0;
	// lines that should be glued together and that are waiting to be printed
	private List<PDFLine> mWaitingLines = new ArrayList<>();

	public PDFDocumentContext(FMDocument pFMDocument, File pSourceDir, PDDocument pPDDocument, PDPage pCurrentPage, PDPageContentStream pContent, float pYPos)
	{
		super();
		mFMDocument = pFMDocument;
		mSourceDir = pSourceDir;
		mDocument = pPDDocument;
		mCurrentPage = pCurrentPage;
		mContent = pContent;
		mYPos = pYPos;
	}

	public PDFDocumentContext createClone()
	{
		final PDFDocumentContext clone = new PDFDocumentContext(mFMDocument, mSourceDir, mDocument, mCurrentPage, mContent, mYPos);
		clone.mChapterNumbers.addAll(mChapterNumbers);
		clone.mCurChapTitle = mCurChapTitle;
		clone.mCurPageNum = mCurPageNum;
		clone.mBookTitle = mBookTitle;
		clone.mChapterPages.addAll(mChapterPages);
		clone.mStopGeneration = mStopGeneration;
		clone.mNotesOnPage.addAll(mNotesOnPage);
		clone.mNotesHeight = mNotesHeight;
		clone.mWaitingLines.addAll(mWaitingLines);
		return clone;
	}

	public FMDocument getFMDocument()
	{
		return mFMDocument;
	}

	public List<PDFLine> getWaitingLines()
	{
		return mWaitingLines;
	}

	public File getSourceDir()
	{
		return mSourceDir;
	}
	public PDDocument getDocument()
	{
		return mDocument;
	}
	public void setDocument(PDDocument pDocument)
	{
		mDocument = pDocument;
	}
	public PDPage getCurrentPage()
	{
		return mCurrentPage;
	}
	public void setCurrentPage(PDPage pCurrentPage)
	{
		mCurrentPage = pCurrentPage;
	}
	public PDPageContentStream getContent()
	{
		return mContent;
	}
	public void setContent(PDPageContentStream pContent)
	{
		mContent = pContent;
	}
	public float getYPos()
	{
		return mYPos;
	}
	public void setYPos(float pYPos)
	{
		mYPos = pYPos;
	}
	public Stack<MutableInt> getChapterNumbers()
	{
		return mChapterNumbers;
	}
	public void setCurPageNum(int pCurPageNum)
	{
		mCurPageNum = pCurPageNum;
	}
	public void setCurChapTitle(String pCurChapTitle)
	{
		mCurChapTitle = pCurChapTitle;
		mChapterPages.add(new AbstractMap.SimpleEntry<String, Integer>(pCurChapTitle, mCurPageNum + 1));
	}
	public void setBookTitle(String pBookTitle)
	{
		mBookTitle = pBookTitle;
	}
	public String getBookTitle()
	{
		return mBookTitle;
	}
	public String getCurChapTitle()
	{
		return mCurChapTitle;
	}
	public int getCurPageNum()
	{
		return mCurPageNum;
	}

	public List<Map.Entry<String, Integer>> getChapterPages()
	{
		return mChapterPages;
	}

	public List<NoteReference> getNotesOnPage()
	{
		return mNotesOnPage;
	}

	public float getNotesHeight()
	{
		return mNotesHeight;
	}

	public void setNotesHeight(float pNotesHeight)
	{
		mNotesHeight = pNotesHeight;
	}

	public boolean isStopGeneration()
	{
		return mStopGeneration;
	}

	public void setStopGeneration(boolean pStopGeneration)
	{
		mStopGeneration = pStopGeneration;
	}
}
