package jyt.freemind.converter.pdf;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import jyt.freemind.converter.ConvUtil;
import jyt.freemind.converter.ConvertMM;
import jyt.freemind.converter.FMDocument;
import jyt.freemind.converter.FMNode;
import jyt.freemind.converter.FMNote;
import jyt.freemind.converter.FMParagraph;
import jyt.freemind.converter.FMParagraph.Property;
import jyt.freemind.converter.Justification;
import jyt.freemind.converter.NoteReference;
import jyt.freemind.converter.NoteReference.NoteRefType;
import jyt.freemind.converter.pdf.PDFLine.PDFLineProperty;
import jyt.freemind.converter.pdf.PDFWord.ImageType;

/**
 * This class builds a PDF file from a FMNode, metadata and a given format.
 * @author jytou
 */
public class PDFBuilder
{
	private final static String DIALOG_DASH = "—";
	private final static int DIALOG_DASH_CODEPOINT = DIALOG_DASH.codePointAt(0);
	private final static Set<Integer> SEPARATORS = " \u00A0,.:;!?\"'“”()[]".codePoints().boxed().collect(Collectors.toSet());

	/**
	 * Builds a PDF file from the given information.
	 * @param pSourceDir
	 * @param pFmNode
	 * @param pMeta
	 * @param pFormat
	 * @param pFileName
	 * @throws IOException
	 */
	public static void buildPDFFile(final FMDocument pDocument, final File pSourceDir, final FMNode pFmNode, final Map<String, String> pMeta, final Map<String, String> pFormat, String pFileName) throws IOException
	{
		// Build the PDF configuration
		final PDFConf conf = new PDFConf(
				ConvUtil.getFloatProp(pFormat, "PageWidth"),
				ConvUtil.getFloatProp(pFormat, "PageHeight"),
				ConvUtil.getFloatProp(pFormat, "TopMargin"),
				ConvUtil.getFloatProp(pFormat, "BottomMargin"),
				ConvUtil.getFloatProp(pFormat, "InnerMargin"),
				ConvUtil.getFloatProp(pFormat, "OuterMargin"),
				ConvUtil.getFloatProp(pFormat, "NormalFontSize"),
				ConvUtil.getFloatProp(pFormat, "Interlines"),
				ConvUtil.getFloatProp(pFormat, "ExtraInterPar"),
				ConvUtil.getIntArrayProp(pFormat, "TitleFontSizes"),
				pMeta,
				ConvUtil.getBooleanProp(pFormat, "ChapterStartOnEven"),
				ConvUtil.getIntProp(pFormat, "MaxPages", Integer.MAX_VALUE),
				ConvUtil.getIntProp(pFormat, "NotesFontSize", 8),
				ConvUtil.getFloatProp(pFormat, "NotesNoteSpacing", 1.7f),
				ConvUtil.getFloatProp(pFormat, "NotesInterval", 1.9f),
				ConvUtil.getFloatProp(pFormat, "NotesDistanceFromText", 1.0f)
			).
				setFontNameMain(ConvUtil.getStringProp(pFormat, PDFFont.MAIN, "LiberationSerif")).
				setFontNameTitles(ConvUtil.getStringProp(pFormat, PDFFont.TITLES, "Helvetica")).
				setFontNameCaptions(ConvUtil.getStringProp(pFormat, PDFFont.CAPTIONS, "Helvetica")).
				setFontNameCitations(ConvUtil.getStringProp(pFormat, PDFFont.CITATIONS, "Helvetica")).
				setFontNameNotes(ConvUtil.getStringProp(pFormat, PDFFont.NOTES, "Helvetica"))
					;
		conf.setHeadersFontSize(ConvUtil.getFloatProp(pFormat, "HeadersFontSize", conf.getNormalFontSize()));
		conf.setFontNameHeaders(ConvUtil.getStringProp(pFormat, PDFFont.HEADERS, conf.getFontNameMain()));

		// Initialize the document and its context
		final PDDocument document = new PDDocument();
		final PDPage currentPage = new PDPage(new PDRectangle(conf.getPageWidth(), conf.getPageHeight()));
		final PDPageContentStream content = new PDPageContentStream(document, currentPage, PDPageContentStream.AppendMode.APPEND, true);

		final PDFDocumentContext context = new PDFDocumentContext(pDocument, pSourceDir, document, currentPage, content, getStartY(conf));
		context.setBookTitle(pMeta.get("Title"));

		// Generate the PDF using the root node, the configuration and the context
		final PDFNotesContext notesContext = new PDFNotesContext(conf.getBottomMargin());
		generatePDF(pFmNode, conf, context, notesContext);

		if ((!context.isStopGeneration()) && (context.getChapterPages().size() > 2))
		// Generate the Table of Contents
			generateTOC(conf, pMeta, context);

		final List<NoteReference> globNotes = notesContext.getGlobNotes();
		if (!globNotes.isEmpty())
		{
			final String notesChapterTitle = ConvertMM.sLangProps.getProperty("notes");
			context.setCurChapTitle(notesChapterTitle);
			changePDFPage(conf, context, notesContext, true);
			FMParagraph paragraph = new FMParagraph(notesChapterTitle, 0, 0.5f);
			paragraph.setProperty(Property.JUSTIFICATION, "center");
			final Stack<PDFFontModifier> modifiers = new Stack<>();
			final PDFParagraphContext pParContext = new PDFParagraphContext(paragraph, conf.getFontNameTitles(), conf.getTitleFontSizes()[1]);
			PDFLine line;
			while ((line = getNextLine(conf, context, pParContext, null)) != null)
				printLine(conf, context, null, line);
			final List<NoteReference> notes = new ArrayList<>();
			final int nbGlobNotes = globNotes.size();
			for (int iNote = 0; iNote < nbGlobNotes; iNote++)
			{
				final NoteReference curNote = globNotes.remove(0);
				notes.add(curNote);
				if (computeAllNotesHeight(conf, context, notes) > conf.getPageHeight() - conf.getTopMargin() - conf.getBottomMargin() - conf.getHeadersFontSize() * 2)
				{
					// this is overflowing, flush the existing ones and create a new page
					notes.remove(notes.size() - 1);
					printNotes(conf, context, notes);
					notes.clear();
					notes.add(curNote);
					changePDFPage(conf, context, notesContext, true);
				}
			}
			if (!notes.isEmpty())
				printNotes(conf, context, notes);
		}
		context.getContent().close();
		context.getDocument().addPage(context.getCurrentPage());

		// Save the final document
		context.getDocument().save(pFileName);
		context.getDocument().close();
	}

	/**
	 * Generate the TOC for the current document.<br>
	 * Note that it is needed to calculate all page numbers before generating the TOC, thus the TOC must be at the end.
	 * @param pConf
	 * @param pMeta 
	 * @param pContext
	 * @throws IOException
	 */
	public static void generateTOC(final PDFConf pConf, Map<String, String> pMeta, final PDFDocumentContext pContext) throws IOException
	{
		String tocName = ConvertMM.sLangProps.getProperty("toc_name");
		pContext.setCurChapTitle(tocName);
		changePDFPage(pConf, pContext, null, false);
		FMParagraph paragraph = new FMParagraph(tocName, 0, 0.5f);
		paragraph.setProperty(Property.JUSTIFICATION, "center");
		final Stack<PDFFontModifier> modifiers = new Stack<>();
		final PDFParagraphContext pParContext = new PDFParagraphContext(paragraph, pConf.getFontNameTitles(), pConf.getTitleFontSizes()[1]);
		PDFLine line;
		while ((line = getNextLine(pConf, pContext, pParContext, null)) != null)
			printLine(pConf, pContext, null, line);
		final PDFont baseTOCFont = PDFFontFactory.getFont(pContext.getDocument(), pConf.getFontNameMain(), false, false);
		final float dotSize = baseTOCFont.getStringWidth(".") / 1000 * pConf.getNormalFontSize() + baseTOCFont.getSpaceWidth() / 1000 * pConf.getNormalFontSize();
		// remove the TOC itself from the list
		pContext.getChapterPages().remove(pContext.getChapterPages().size() - 1);
		for (Map.Entry<String, Integer> chapterPage : pContext.getChapterPages())
		{
			float w = baseTOCFont.getStringWidth(chapterPage.getKey().replace(" ", "\u00A0\u00A0") + " " + chapterPage.getValue()) / 1000 * pConf.getNormalFontSize();
			paragraph = new FMParagraph(chapterPage.getKey().replace(" ", "\u00A0\u00A0") + dots((pConf.getPageWidth() - pConf.getInnerMargin() - pConf.getOuterMargin() - w) / dotSize + 1) + " " + chapterPage.getValue(), 0, 0);
			final PDFParagraphContext paragraphContext = new PDFParagraphContext(paragraph, pConf.getFontNameMain(), pConf.getNormalFontSize());
			while ((line = getNextLine(pConf, pContext, paragraphContext, null)) != null)
			{
				line.setJustification(Justification.JUSTIFIED);
				PDFBuilder.printLine(pConf, pContext, null, line);
			}
		}
//			context.setCurChapTitle("");
//			changePDFPage(conf, context, true);
	}

	/**
	 * Returns an amount of space+dot for the TOC.
	 * @param n
	 * @return
	 */
	static String dots(float n)
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < n-2; i++)
			sb.append(" .");
		return sb.toString();
	}

	/**
	 * Main generation of the biggest part of the document.
	 * @param pNode
	 * @param pConf
	 * @param pContext
	 * @param pNotesContext
	 * @throws IOException
	 */
	private static void generatePDF(FMNode pNode, PDFConf pConf, PDFDocumentContext pContext, final PDFNotesContext pNotesContext) throws IOException
	{
		pNode.setStartPage(pContext.getCurPageNum());
		final Stack<PDFFontModifier> modifiers = new Stack<>();
		final int nodeLevel = ConvUtil.getNodeLevel(pNode);
		String label = pNode.getLabel();
		if (label.startsWith("##"))
		// second level chapter titles
			label = "" + pContext.getChapterNumbers().peek().incrementAndGet() + ". " + label.substring(2);
		else if (label.startsWith("#"))
		// first level chapter titles
			label = MessageFormat.format(ConvertMM.sLangProps.getProperty("chapter"), pContext.getChapterNumbers().peek().incrementAndGet(), label.substring(1));
		if (nodeLevel == 1)
		// level 2 are chapters that need a full new page turn
		{
			// print any waiting line
			final List<PDFLine> wl = new ArrayList<>(pContext.getWaitingLines());
			pContext.getWaitingLines().clear();
			flushWaitingLines(pConf, pContext, pNotesContext, wl);

			// This is a new chapter
			if (pConf.isChaptersStartOnEven() && (pContext.getCurPageNum() % 2 == 1))
			// We want to start every chapter on even pages but it would start on an odd page - we have to skip a page
			{
				changePDFPage(pConf, pContext, pNotesContext, true);
				if (!label.isEmpty())
					pContext.setCurChapTitle(label);
				changePDFPage(pConf, pContext, null, false);
			}
			else // no skipping pages
			{
				if (!label.isEmpty())
					pContext.setCurChapTitle(label);
				changePDFPage(pConf, pContext, pNotesContext, false);
			}
		}
		if (!label.isEmpty())
		{
			if (nodeLevel == 0)
			// This is the title page, add a page with the basic editor information
			{
				FMParagraph paragraph = new FMParagraph(pConf.getMetas().get("Author"), 0, 0);
				paragraph.setProperty(Property.JUSTIFICATION, "center");
				PDFLine line;
				PDFParagraphContext pc = new PDFParagraphContext(paragraph, pConf.getFontNameMain(), pConf.getTitleFontSizes()[pConf.getTitleFontSizes().length - 1]);
				while ((line = getNextLine(pConf, pContext, pc, null)) != null)
					printLine(pConf, pContext, null, line);
				paragraph = new FMParagraph(pConf.getMetas().get("Title"), 0, 0);
				paragraph.setProperty(Property.JUSTIFICATION, "center");
				pc = new PDFParagraphContext(paragraph, pConf.getFontNameMain(), pConf.getTitleFontSizes()[0]);
				while ((line = getNextLine(pConf, pContext, pc, null)) != null)
				{
					line.setJustification(Justification.CENTER);
					pContext.setYPos(pConf.getPageHeight() / 2 + pConf.getTitleFontSizes()[0]);
					printLine(pConf, pContext, null, line);
				}
			}
			else
			{
				// This is a chapter with a title. Let's center the whole text - which should ideally fit on a single page.
				final FMParagraph paragraph = new FMParagraph(label, 0, 0.5f);
				final PDFParagraphContext pc = new PDFParagraphContext(paragraph, pConf.getFontNameTitles(), pConf.getTitleFontSizes()[nodeLevel]);
				PDFLine line;
				while ((line = getNextLine(pConf, pContext, pc, null)) != null)
				{
					if (nodeLevel == 1)
						line.setJustification(Justification.CENTER);
					applyTitleProperties(line, paragraph, nodeLevel);
					pContext.setYPos((float)(pContext.getYPos() - pConf.getNormalFontSize() * 1));
//					if (pContext.getYPos() - getLinesHeight(pConf, pContext, lines) - (pConf.getInterline() + pConf.getExtraInterPar() + 1) * pConf.getNormalFontSize() < pConf.getBottomMargin())
//					{
//						changePDFPage(pConf, pContext, null, true);
//						if (pContext.isStopGeneration())
//							return;
//					}
					printLine(pConf, pContext, pNotesContext, line);
				}
				pContext.setYPos((float)(pContext.getYPos() - pConf.getNormalFontSize() * (pConf.getInterline() - 1) + (paragraph.getSpaceBefore() + paragraph.getSpaceAfter()) * PDFFontFactory.getFont(pContext.getDocument(), pConf.getFontNameTitles(), true, false).getHeight(pConf.getTitleFontSizes()[nodeLevel])));
			}
		}
		if (pNode.getParagraphs() != null)
		// the current node has some text content, add it!
		{
			boolean alreadyVJust = false;
			for (int i = 0; i < pNode.getParagraphs().size(); i++)
			{
				final FMParagraph paragraph = pNode.getParagraphs().get(i);
				if (paragraph.getProperties().isEmpty())
				{
					// deal with dialogues
					final String txt = paragraph.getText();
					if (txt.startsWith("−") || txt.startsWith("—") || txt.startsWith("-"))
					{
						if ((txt.length() > 1) && (txt.codePointAt(1) != 160))
							paragraph.setText(DIALOG_DASH + "\u00A0" + txt.substring(txt.codePointAt(1) == ' ' ? 2 : 1));
						else if (txt.codePointAt(0) != DIALOG_DASH_CODEPOINT)
							paragraph.setText(DIALOG_DASH + txt.substring(1));
					}
				}

				String fontName = pConf.getFontNameMain();
				float fontSize = pConf.getNormalFontSize();
				final boolean wasItalic = modifiers.contains(PDFFontModifier.ITALIC);
				if (paragraph.hasProperty(Property.LIST))
					paragraph.setProperty(Property.PAR_INDENT, pConf.getNormalFontSize() * 2);
				if (paragraph.hasProperty(Property.CAPTION))
				{
					fontName = pConf.getFontNameCaptions();
					fontSize -= 2;
					modifiers.add(PDFFontModifier.ITALIC);
				}
				else if (!paragraph.hasProperty(Property.CITATION))
					paragraph.setProperty(Property.FIRST_INDENT, 12f);// Default indent is 12f
				// Calculate the available area in which we can print something
				float availablePageWidth = pConf.getPageWidth() - pConf.getInnerMargin() - pConf.getOuterMargin();
				if (paragraph.hasProperty(Property.CITATION))
				// Reduce it for quotes
					availablePageWidth -= Math.max(pConf.getInnerMargin(), pConf.getOuterMargin()) * 2;
				if ((!alreadyVJust) && paragraph.hasProperty(Property.VJUST))
				// Oh, we have to center this vertically - we have to explore all the vertically centered paragraphs to know where to start
				{
					alreadyVJust = true;
					float totHeight = 0;
					int iCurPar = i;
					while ((iCurPar < pNode.getParagraphs().size()) && pNode.getParagraphs().get(iCurPar).hasProperty(Property.VJUST))
					{
						FMParagraph curPar = pNode.getParagraphs().get(iCurPar);
						PDFParagraphContext paragraphContext = new PDFParagraphContext(curPar, fontName, fontSize);
						paragraphContext.getModifiersAsSet().addAll(modifiers);
						PDFLine[] lines = getPDFLines(pConf, pContext, paragraphContext, pNotesContext);
						totHeight += getLinesHeight(pConf, pContext, lines);
						modifiers.clear();
						modifiers.addAll(paragraphContext.getModifiersAsSet());
						iCurPar++;
					}
					float diff = pConf.getPageHeight() - pConf.getTopMargin() - pConf.getBottomMargin() - totHeight / 2;
					// Now we know where to start
					pContext.setYPos(pConf.getBottomMargin() + pConf.getPageHeight() / 2 + totHeight / 2);
				}
				// Retrieve the lines for this paragraph...
				PDFParagraphContext paragraphContext = new PDFParagraphContext(paragraph, fontName, fontSize);
				paragraphContext.getModifiersAsSet().addAll(modifiers);
				PDFLine line;
				float y = pContext.getYPos();
				while ((line = getNextLine(pConf, pContext, paragraphContext, pNotesContext)) != null)
				// ... print them on paper
					printLine(pConf, pContext, pNotesContext, line);
				// and go forward in the page
				pContext.setYPos(pContext.getYPos() - pConf.getExtraInterPar() * pConf.getNormalFontSize());
				if (paragraph.hasProperty(Property.CAPTION) && (!wasItalic))
					modifiers.remove(PDFFontModifier.ITALIC);
				if (paragraph.hasProperty(Property.CADRE))
				{
					final boolean citation = paragraph.hasProperty(Property.CITATION);
					pContext.getContent().addRect(getLeftMargin(pConf, pContext, citation, false), y + 1f, pConf.getPageWidth() - getInnerMargin(pConf, citation, false) - getOuterMargin(pConf, citation, false), pContext.getYPos() - y);
					pContext.getContent().stroke();
				}
			}
		}
		// Now recursively generate the children
		pContext.getChapterNumbers().push(new MutableInt(0));
		for (FMNode child : pNode.getChildren())
		{
			generatePDF(child, pConf, pContext, pNotesContext);
			if (pContext.isStopGeneration())
				return;
		}
		pContext.getChapterNumbers().pop();
	}

	private static void applyTitleProperties(PDFLine pLine, FMParagraph pParagraph, int pNodeLevel)
	{
		pLine.addGluedWith(PDFLineProperty.FIRST_INDENT);
		pLine.addGluedWith(PDFLineProperty.FIRST_OF_LIST);
		pLine.addGluedWith(PDFLineProperty.IMAGE);

		switch (pNodeLevel)
		{
		case 1:
			pParagraph.setProperty(Property.TITLE1, true);
			pLine.addGluedWith(PDFLineProperty.TITLE2);
			break;
		case 2:
			pParagraph.setProperty(Property.TITLE2, true);
			pLine.addGluedWith(PDFLineProperty.TITLE3);
			break;
		case 3:
			pParagraph.setProperty(Property.TITLE3, true);
			pLine.addGluedWith(PDFLineProperty.TITLE4);
			break;
		case 4:
			pParagraph.setProperty(Property.TITLE4, true);
			pLine.addGluedWith(PDFLineProperty.TITLE5);
			break;
		case 5:
			pParagraph.setProperty(Property.TITLE5, true);
			break;

		default:
			throw new RuntimeException("Level of paragraph " + pNodeLevel + "is not supported");
		}
	}

	/**
	 * Returns the total height for these lines
	 * @param pConf
	 * @param pContext
	 * @param pLines
	 * @return
	 * @throws IOException
	 */
	public static float getLinesHeight(PDFConf pConf, PDFDocumentContext pContext, PDFLine[] pLines) throws IOException
	{
		float linesH = 0;
		for (PDFLine pdfLine : pLines)
		{
			float lineHeight = -1;
			for (PDFWord word : pdfLine.getWords())
			{
//				float h = (float)(PDFFontFactory.getFont(pContext.getDocument(), word).getFontDescriptor().getCapHeight() / 1000 * word.getPoints() * pConf.getInterline());
				float h = getPDFWordYSpacing(word, pConf);
				if (linesH < h)
					linesH = h;
			}
			linesH += lineHeight;
		}
		return linesH;
	}

	/**
	 * Returns the vertical space taken by this word - including if it's an image.
	 * !!! Works only for normal text, NOT FOR TITLES !!!
	 * @param pWord
	 * @param pConf
	 * @return
	 */
	static float getPDFWordYSpacing(PDFWord pWord, PDFConf pConf)
	{
		if (ImageType.FULL_WIDTH.equals(pWord.getImageType()))
			return pWord.getPoints() * (pConf.getPageWidth() - pConf.getInnerMargin() - pConf.getOuterMargin()) / pWord.getWidth() + pConf.getNormalFontSize();
		else if (ImageType.INLINE.equals(pWord.getImageType()))
			return pWord.getPoints();
		else
			return pWord.getPoints() * (0 + pConf.getInterline());
	}

	/**
	 * Returns the horizontal space taken by this word in the current context
	 * @param pContext
	 * @param pWord
	 * @return
	 * @throws IOException
	 */
	public static float getSpacingForWord(PDFDocumentContext pContext, PDFWord pWord, PDFConf pConf) throws IOException
	{
		if (ImageType.NONE.equals(pWord.getImageType()))
		// a normal word
			return PDFFontFactory.getFont(pContext.getDocument(), pWord).getSpaceWidth() / 2 / 1000 * pWord.getPoints();
		else if (ImageType.FULL_WIDTH.equals(pWord.getImageType()))
		// an image in full page
			return pConf.getPageWidth() - pConf.getInnerMargin() - pConf.getOuterMargin();
		else
		// an inline image
			return pWord.getWidth();
	}

	/**
	 * Turns a new page in the document. Adds the header if asked for it.
	 * @param pConf
	 * @param pContext
	 * @param pShowHeader
	 * @throws IOException
	 */
	private static void changePDFPage(PDFConf pConf, PDFDocumentContext pContext, PDFNotesContext pNotesContext, boolean pShowHeader) throws IOException
	{
		if (pContext.isStopGeneration())
			return;

		List<NoteReference> notesOnPage = pNotesContext == null ? Collections.emptyList() : pNotesContext.getFootNotes();
		if (!notesOnPage.isEmpty())
		{
			notesOnPage = new ArrayList<>(notesOnPage);
			pContext.getNotesOnPage().clear();
			// make sure to clear any waiting lines in the context before printing the notes, those waiting lines should only be printed on the next page
			final List<PDFLine> waitingLines = new ArrayList<>(pContext.getWaitingLines());
			pContext.getWaitingLines().clear();
			if (pNotesContext != null)
			{
				pNotesContext.setCurrentPageBottomSpace(pConf.getBottomMargin());
				pNotesContext.getFootNotes().clear();
			}
			final float allNotesHeight = computeAllNotesHeight(pConf, pContext, notesOnPage);
			log("" + allNotesHeight + " at " + notesOnPage.get(0).getNoteId());
			pContext.setYPos(pConf.getBottomMargin() + allNotesHeight);

			printNotes(pConf, pContext, notesOnPage);
			pContext.getWaitingLines().addAll(waitingLines);
		}
		// Make sure that waiting lines have their position reset since we are switching to a new page
		for (PDFLine waitingLine : pContext.getWaitingLines())
			waitingLine.setYPos(-1);

//		pContext.getContent().moveTo(0, pConf.getBottomMargin());
//		pContext.getContent().lineTo(pConf.getPageWidth(), pConf.getBottomMargin());
//		pContext.getContent().stroke();

		// Finish the current page
		pContext.getContent().close();
		pContext.getDocument().addPage(pContext.getCurrentPage());
		pContext.setCurrentPage(new PDPage(new PDRectangle(pConf.getPageWidth(), pConf.getPageHeight())));

		pContext.setCurPageNum(pContext.getCurPageNum() + 1);
		if (pContext.getCurPageNum() > pConf.getMaxPages())
		{
			pContext.setStopGeneration(true);
			return;
		}
		final PDPageContentStream content = new PDPageContentStream(pContext.getDocument(), pContext.getCurrentPage(), PDPageContentStream.AppendMode.APPEND, true);
		pContext.setContent(content);

		// We don't want waiting lines to pollute the header, remove them first and keep them for later
		final ArrayList<PDFLine> waitingLines = new ArrayList<>(pContext.getWaitingLines());
		pContext.getWaitingLines().clear();

		if (pShowHeader)
		// Show the header of the page
		{
			if (pContext.getCurChapTitle() != null)
			// Put the chapter/book title if there is one
			{
				FMParagraph paragraph = new FMParagraph(pContext.getCurPageNum() % 2 == 0 ? pContext.getBookTitle() : pContext.getCurChapTitle(), 0, 0);
				paragraph.setProperty(Property.JUSTIFICATION, "center");
				Stack<PDFFontModifier> modifiers = new Stack<>();
				modifiers.push(PDFFontModifier.ITALIC);
//				pContext.setYPos(pConf.getPageHeight() - 18);
				pContext.setYPos(pConf.getPageHeight() - Math.min(pConf.getTopMargin() - pConf.getHeadersFontSize() * 1.7f, 18f));
				final PDFParagraphContext paragraphContext = new PDFParagraphContext(paragraph, pConf.getFontNameHeaders(), pConf.getHeadersFontSize() - 1);
				printLine(pConf, pContext, null, getNextLine(pConf, pContext, paragraphContext, null));
			}
			if (pContext.getCurPageNum() > 0)
			// Add the page number
			{
				FMParagraph paragraph = new FMParagraph(String.valueOf(pContext.getCurPageNum()), 0, 0);
				if (pContext.getCurPageNum() % 2 == 1)
				// odd numbers to the right, even numbers to the left
					paragraph.setProperty(Property.JUSTIFICATION, "right");
				Stack<PDFFontModifier> modifiers = new Stack<>();
				modifiers.push(PDFFontModifier.ITALIC);
//				pContext.setYPos(pConf.getPageHeight() - 18);
				pContext.setYPos(pConf.getPageHeight() - Math.min(pConf.getTopMargin() - pConf.getHeadersFontSize() * 1.7f, 18f));
				final PDFParagraphContext paragraphContext = new PDFParagraphContext(paragraph, pConf.getFontNameHeaders(), pConf.getHeadersFontSize() - 1);
				printLine(pConf, pContext, null, getNextLine(pConf, pContext, paragraphContext, null));
			}
		}
		pContext.setYPos(getStartY(pConf));
		flushWaitingLines(pConf, pContext, pNotesContext, waitingLines);
	}

	private static void flushWaitingLines(PDFConf pConf, PDFDocumentContext pContext, PDFNotesContext pNotesContext, final List<PDFLine> pWaitingLines) throws IOException
	{
		if (!pWaitingLines.isEmpty())
		{
			for (int i = 0; i < pWaitingLines.size(); i++)
			{
				final PDFLine line = pWaitingLines.get(i);
				line.clearGluedWith();
				if (line.getYPos() != -1)
					pContext.setYPos(line.getYPos());
				if ((line.getLineProperties().contains(PDFLineProperty.IMAGE)) && (i < pWaitingLines.size() - 1) && (pWaitingLines.get(i + 1).getLineProperties().contains(PDFLineProperty.CAPTION)))
					line.setSpaceAfter(0);
				printLine(pConf, pContext, pNotesContext, line);
			}
		}
	}

	private static void printNotes(PDFConf pConf, PDFDocumentContext pContext, List<NoteReference> pNotes) throws IOException
	{
		float noteIndicatorWidth = computeMaxNoteIndicatorWidth(pConf, pContext, pNotes);
		boolean first = true;
		for (NoteReference noteRef : pNotes)
		{
			if (first)
			{
				if (!NoteRefType.GLOBAL.equals(noteRef.getType()))
				// For global notes, we start at the beginning of the page, not at the end
					pContext.setYPos(computeAllNotesHeight(pConf, pContext, pNotes) + pConf.getBottomMargin());
				first = false;
			}
			final float oldYPosition = pContext.getYPos();
			// first print the note indicator
			printLine(pConf, pContext, null,
					//new PDFParagraphContext(new FMParagraph(noteRef.getRefId(), 0f, 0f), pConf.getFontNameNotes(), pConf.getNoteFontSize()),
					new PDFLine(new PDFWord[] { new PDFWord(pConf.getFontNameNotes(), 100, 0, pConf.getNoteFontSize(), noteRef.getRefId(), Collections.emptyList(), true) }, false, false));
			pContext.setYPos(oldYPosition);
			final Stack<PDFFontModifier> modifiers = new Stack<>();
			for (FMParagraph noteParagraph : pContext.getFMDocument().getNotes().get(noteRef.getNoteId()).getContents())
			{
				noteParagraph.setProperty(Property.PAR_INDENT, noteIndicatorWidth);
				PDFParagraphContext pc = new PDFParagraphContext(noteParagraph, pConf.getFontNameNotes(), pConf.getNoteFontSize());
				PDFLine line;
				PDFNotesContext notesContext;
//					final PDFConf notesConf = new PDFConf(pConf.getPageWidth(), pConf.getPageHeight(), pConf.getTopMargin(), pConf.getBottomMargin(), pConf.getInnerMargin(), pConf.getOuterMargin(), pConf.getNoteFontSize(), pConf.getNoteInterline(), pConf.getNoteInterline() * 0.3f, pConf.getTitleFontSizes(), pConf.getMetas(), pConf.isChaptersStartOnEven(), pConf.getMaxPages(), pConf.getNoteFontSize(), pConf.getNoteNoteSpacing(), pConf.getNoteInterline(), pConf.getNotesDistanceFromText());
				final PDFConf notesConf = pConf.clone();
				notesConf.setNormalFontSize(pConf.getNoteFontSize());
				notesConf.setInterline(pConf.getNoteInterline());
				notesConf.setExtraInterPar(pConf.getNoteInterline() * 0.3f);
				while ((line = getNextLine(notesConf, pContext, pc, notesContext = new PDFNotesContext(pConf.getBottomMargin()))) != null)
					printLine(notesConf, pContext, notesContext, line);
			}
			pContext.setYPos(pContext.getYPos() - pConf.getNoteNoteSpacing());
		}
	}

	private static float computeMaxNoteIndicatorWidth(PDFConf pConf, PDFDocumentContext pContext, List<NoteReference> pNotesOnPage) throws IOException
	{
		float w = 0;
		final PDFont currentFont = PDFFontFactory.getFont(pContext.getDocument(), pConf.getFontNameNotes(), new HashSet<>());
		for (NoteReference noteReference : pNotesOnPage)
			w = Math.max(w, currentFont.getStringWidth(noteReference.getRefId() + "  ") / 1000f * PDFFontFactory.getSize(pConf.getNoteFontSize(), false));
		return w;
	}

	/**
	 * Returns the default starting Y for a new page
	 * @param pConf
	 * @return
	 */
	public static float getStartY(PDFConf pConf)
	{
		return pConf.getPageHeight() - pConf.getTopMargin();
	}

	private static PDFLine[] getPDFLines(final PDFConf pConf, final PDFDocumentContext pContext, final PDFParagraphContext pParagraphContext, final PDFNotesContext pNotesContext) throws IOException
	{
		final List<PDFLine> lines = new ArrayList<>();
		PDFLine line;
		while ((line = getNextLine(pConf, pContext, pParagraphContext, pNotesContext)) != null)
			lines.add(line);
		return lines.toArray(new PDFLine[lines.size()]);
	}

	/**
	 * Returns the next PDF line for a given paragraph.context
	 * It calculates the positions of words depending on the font size, justification, etc. and makes sure that every line can fit in the current page. Words are not positioned on the page yet, though.
	 * @param pConf
	 * @param pContext
	 * @param pPageWidth
	 * @param pFont
	 * @param pFontSize
	 * @param pParagraph
	 * @param pModifiers
	 * @return the next line that has been computed, on <code>null</code> if the next line is empty for this paragraph
	 * @throws IOException
	 */
	private static PDFLine getNextLine(final PDFConf pConf, final PDFDocumentContext pContext, final PDFParagraphContext pParagraphContext, final PDFNotesContext pNotesContext) throws IOException
	{
		boolean isFirstLine = false;
		boolean isFirstWord = false;
		float posx = pParagraphContext.getParIndent();
		if (pParagraphContext.getCurPos() == 0)
		// we're at the beginning of the paragraph, we need to make some initial verifications
		{
			isFirstLine = true;
			isFirstWord = true;
			if (pParagraphContext.getParagraph().hasProperty(Property.FIRST_INDENT))
				posx += (float)pParagraphContext.getParagraph().getPropertyValue(Property.FIRST_INDENT);
			// Replace ISBN and potentially discard the paragraph
			if (pParagraphContext.getTxt().contains(ConvUtil.ISBN_TAG))
				if (pConf.getMetas().containsKey(ConvUtil.KEY_ISBN))
				// substitute the tag for its actual value
					pParagraphContext.setTxt(pParagraphContext.getTxt().replace(ConvUtil.ISBN_TAG, pConf.getMetas().get(ConvUtil.KEY_ISBN)));
				else // no ISBN, just dismiss the paragraph
					return null;

			pParagraphContext.setCurrentFont(PDFFontFactory.getFont(pContext.getDocument(), pParagraphContext.getFontName(), pParagraphContext.getModifiersAsSet()));
		}

		final List<PDFWord> currentWords = new ArrayList<>();
		// the words that we haven't consumed yet because they are glued to the current word
		final List<PDFWord> pendingWords = new ArrayList<>();
		// the number of tokens we have consumed to build 
		int nbPendingToks = 0;

		final float outerMargin = getOuterMargin(pConf, pParagraphContext.isCitation(), pParagraphContext.isCadre());
		final float innerMargin = getInnerMargin(pConf, pParagraphContext.isCitation(), pParagraphContext.isCadre());

		int currentFootNoteNumber = pNotesContext == null ? 0 : pNotesContext.getFootNotes().size();
		int currentChapterNoteNumber = pNotesContext == null ? 0 : pNotesContext.getChapNotes().size();
		int currentGlobalNoteNumber = pNotesContext == null ? 0 : pNotesContext.getGlobNotes().size();
		String s;
		while ((s = pParagraphContext.curToken()) != null)
		{
//			System.out.println("Token \"" + s + "\"");
			if ((!s.isEmpty()) && (s.startsWith("<")))
			// Some more text to process with a tag
			{
				//.replace((char)0xa0, ' ').replace((char)0x2212, '-');
				final char endingChar = '>';
				int pos = 1;// skip the <
				boolean ending = false;// true if tag end
				if ((s.length() > pos) && (s.charAt(pos) == '/'))
				// This is a tag's end
				{
					ending = true;
					pos++;
				}
				final int supPos = s.indexOf(endingChar, pos);// position of >
				if (supPos >= 0)
				// There is the closing of the tag
				{
					final String tagFull = s.substring(pos, supPos);
					// Only the tag itself without its properties
					final String tag = tagFull.contains(" ") ? tagFull.substring(0, tagFull.indexOf(' ')) : tagFull;

					// Now grab the properties
					final Map<String, String> props = new HashMap<>();
					int ppos = tag.length() + 1;
					while (ppos < tagFull.length())
					{
						final int eqpos = tagFull.indexOf('=', ppos);
						if (eqpos > 0)
						{
							final String propName = tagFull.substring(ppos, eqpos);
							ppos = eqpos + 2;
							final int quotpos = tagFull.indexOf('"', ppos);
							if (quotpos >= ppos)
							{
								final String propValue = tagFull.substring(ppos, quotpos);
								props.put(propName, propValue);
								ppos = quotpos + 2;
							}
							else
								throw new RuntimeException("Could not find end of quote in tag " + tagFull);
						}
						else
							throw new RuntimeException("Could not find equal in tag " + tagFull);
					}

					// Process known tags
					if ("i".equals(tag))// italic
						alterModifiers(pParagraphContext, PDFFontModifier.ITALIC, ending);
					else if ("b".equals(tag))// bold
						alterModifiers(pParagraphContext, PDFFontModifier.BOLD, ending);
					else if ("sup".equals(tag))
						alterModifiers(pParagraphContext, PDFFontModifier.SUPERSCRIPT, ending);
					else if ("a".equals(tag))
						alterModifiers(pParagraphContext, PDFFontModifier.LINK, ending);
					else if ("font".equals(tag))
						;// ignore for now
					else if ("img".equals(tag))
					{
						if (!ending)
						{
							// An image - get its src property
							final String src = props.get("src");
							if (src != null)
							{
								// Fetch the image and make sure it exists - grab its size and set it in the word, create a line from it
								final File image = new File(pContext.getSourceDir().getAbsolutePath() + "/" + src);
								if (image.exists())
								{
									final BufferedImage bufferedImage = ImageIO.read(image);
									final int w = bufferedImage.getWidth();
									final int h = bufferedImage.getHeight();
									if (!props.containsKey("width"))
									// it is not an embedded image but on its own line
									{
										if (!currentWords.isEmpty())
										// we have to serve the current words encountered in the previous line if there are some
										{
											final PDFLine pdfLine = new PDFLine(currentWords.toArray(new PDFWord[currentWords.size()]), pParagraphContext.isCitation(), pParagraphContext.isCadre());
											if (!pdfLine.getJustification().equals(pParagraphContext.getJustification()))
												pdfLine.setJustification(pParagraphContext.getJustification());
											return pdfLine;
										}
										else
										{
											pParagraphContext.incCurPos();
											final PDFLine pdfLine = new PDFLine(new PDFWord[] {new PDFWord(w, h, src, ImageType.FULL_WIDTH, true)}, pParagraphContext.isCitation(), pParagraphContext.isCadre());
											pdfLine.addGluedWith(PDFLineProperty.CAPTION);
											pdfLine.addLineProperty(PDFLineProperty.IMAGE);
											pdfLine.setSpaceBefore(1);
											pdfLine.setSpaceAfter(1);
											return pdfLine;
										}
									}
									else
									{
										// Add the image as a word
										final String propWidth = props.get("width");
										float width;
										float height;
										if (propWidth.endsWith("%"))
										{
											width = (pConf.getPageWidth() - innerMargin - outerMargin) * Float.valueOf(propWidth.substring(0, propWidth.length()-1)) / 100;
											height = width * h / w;
										}
										else
										{
											width = Float.valueOf(propWidth) / 2f;
											height = Float.valueOf(props.get("height")) / 2f;
										}
										if (posx + width + pParagraphContext.getSpaceWidth() > pConf.getPageWidth() - innerMargin - outerMargin)
										// this is too much for this line, serve the line, we'll deal with this image later
										{
											final PDFLine pdfLine = new PDFLine(currentWords.toArray(new PDFWord[currentWords.size()]), pParagraphContext.isCitation(), pParagraphContext.isCadre());
											addParagraphPropertiesToLine(pParagraphContext, pdfLine);
											return pdfLine;
										}
										// it fits in the line, increment the position
										posx += width + pParagraphContext.getSpaceWidth();
										currentWords.add(new PDFWord(width, height, src, ImageType.INLINE, currentWords.isEmpty()));
//										pParagraphContext.incCurPos();
									}
								}
								else
									throw new RuntimeException("Could not find image " + src);
							}
							else
								throw new RuntimeException("Could not find source of image " + tagFull);
						}
					}
					else if ("br".equals(tag))
					{
						if (!ending)
						{
							// break the line prematurely
							final PDFLine pdfLine = new PDFLine(currentWords.toArray(new PDFWord[currentWords.size()]), pParagraphContext.isCitation(), pParagraphContext.isCadre());
							addParagraphPropertiesToLine(pParagraphContext, pdfLine);
							if (pParagraphContext.getParIndent() > 0)
								pdfLine.setIndent(pParagraphContext.getParIndent());
							pdfLine.setJustification(Justification.LEFT);
							pParagraphContext.incCurPos();
							return pdfLine;
						}
					}
					else
						throw new RuntimeException("Unsupported tag " + tag);

					// Adapt to the new situation (bold, etc.)
					pParagraphContext.setCurrentFont(PDFFontFactory.getFont(pContext.getDocument(), pParagraphContext.getFontName(), pParagraphContext.getModifiersAsSet()));
					pos = supPos + 1;
					pParagraphContext.autoComputeSpaceWidth();
				}
				else
					throw new RuntimeException("Malformed non-closed tag in paragraph: " + pParagraphContext.getTxt());
			}
			else if (ConvUtil.DELIMS.indexOf(s.charAt(0)) >= 0)
			{
				// It is a delimiter
				if (!currentWords.isEmpty())
					currentWords.get(currentWords.size() - 1).setSpaceAfterWord(true);
			}
			else
			{
				int brackPos;
				final List<NoteReference> wordNoteReferences = new ArrayList<>();
				while ((brackPos = s.indexOf('{')) != -1)
				{
					final int closingBrackPos = s.indexOf('}', brackPos + 1);
					if (closingBrackPos == -1)
						throw new RuntimeException("Could not find closing bracket at " + s);
					final String inside = s.substring(brackPos + 1, closingBrackPos);
					NoteRefType noteRefType;
					String noteMarker;
					if (inside.startsWith("foot-note:") || inside.startsWith("fn:"))
					{
						noteRefType = NoteRefType.FOOT;
						noteMarker = StringUtils.repeat("*", ++currentFootNoteNumber);
					}
					else if (inside.startsWith("chap-note:") || inside.startsWith("cn:"))
					{
						noteRefType = NoteRefType.CHAPTER;
						noteMarker = StringUtils.repeat("#", ++currentChapterNoteNumber);
					}
					else if (inside.startsWith("glob-note:") || inside.startsWith("gn:"))
					{
						noteRefType = NoteRefType.GLOBAL;
						noteMarker = String.valueOf(++currentGlobalNoteNumber);
					}
					else
						throw new RuntimeException("Unknown reference type: " + inside);
					final String noteId = s.substring(s.indexOf(':') + 1, s.indexOf('}'));
					final NoteReference noteReference = new NoteReference(noteMarker, noteId, noteRefType);
					wordNoteReferences.add(noteReference);
					// TODO problem is that this is not reflected in the context
					s = (brackPos > 0 ? s.substring(0, brackPos) : "") + (closingBrackPos < s.length() - 1 ? s.substring(closingBrackPos + 1) : "");
				}

				// It is a real word
				float wordLength = 0;
				try
				{
					wordLength = (pParagraphContext.getCurrentFont().getStringWidth(s) / 1000) * PDFFontFactory.getSize(pParagraphContext.getFontSize(), pParagraphContext.getModifiersAsSet());
				}
				catch (RuntimeException e)
				{
					System.out.println("Was processing the following String: " + s);
					throw e;
				}
				float notesWidth = 0;
				if (!wordNoteReferences.isEmpty())
				{
					final PDFont noteFont = PDFFontFactory.getFont(pContext.getDocument(), pConf.getFontNameNotes(), false, false);
					// The current space width - do not take into account the changes with oblique etc as the space actually doesn't really change
					final float notePoints = PDFFontFactory.getSize(pParagraphContext.getFontSize(), true);
					float noteSepLength = (pParagraphContext.getCurrentFont().getStringWidth(",") / 1000) * notePoints;
					for (NoteReference noteReference : wordNoteReferences)
					{
						if (notesWidth > 0)
							notesWidth += noteSepLength;
						notesWidth += (noteFont.getStringWidth(noteReference.getRefId()) / 1000) * notePoints;
					}
				}
				// If there will be a space after it
				float wordLengthWithSpace = wordLength + pParagraphContext.getSpaceWidth() + notesWidth;
				// Check whether the new word with a space before it reaches the end of the printable area
				if (posx + wordLengthWithSpace > pConf.getPageWidth() - outerMargin - innerMargin)
				// It does, we have to change line
				{
					if (currentWords.isEmpty())
					// uh-oh, we have no words but we have to go to the next line? This means the word is too long and doesn't fit on the page
						throw new RuntimeException("Word too long: " + s);

					// Create the line until the held words
					final PDFLine pdfLine = new PDFLine(currentWords.toArray(new PDFWord[currentWords.size()]), pParagraphContext.isCitation(), pParagraphContext.isCadre());
					addParagraphPropertiesToLine(pParagraphContext, pdfLine);
					if (pParagraphContext.getParIndent() > 0)
						pdfLine.setIndent(pParagraphContext.getParIndent());

					// Get ready for a new line
					if (!pdfLine.getJustification().equals(pParagraphContext.getJustification()))
						pdfLine.setJustification(pParagraphContext.getJustification());
					if (isFirstLine)
					{
						pdfLine.setSpaceBefore(pParagraphContext.getParagraph().getSpaceBefore());
						if (pParagraphContext.getParagraph().getProperties().contains(Property.LIST))
						{
							pdfLine.addLineProperty(PDFLineProperty.FIRST_OF_LIST);
 							pdfLine.setIndent(pConf.getListParIndent());
						}
						else if (pParagraphContext.getParagraph().hasProperty(Property.FIRST_INDENT))
							pdfLine.setIndent((float)pParagraphContext.getParagraph().getPropertyValue(Property.FIRST_INDENT));
					}
					//pParagraphContext.setCurPos(brackPos);
					return pdfLine;
				}
				if (currentWords.isEmpty())
				// No words, set the start after the current word + the indent if required
					posx =
						(pParagraphContext.getParagraph().hasProperty(Property.FIRST_INDENT) ? (float)pParagraphContext.getParagraph().getPropertyValue(Property.FIRST_INDENT) : 0f) +
						pParagraphContext.getParIndent() +
						wordLength;
					//l = wordLength;// do not take the indent for further lines of the paragraph!
				else
				// There were previous words, add the size of the new one
					posx += wordLengthWithSpace;

				// Add the new word in the list of words
				currentWords.add(new PDFWord(pParagraphContext.getFontName(), pParagraphContext.getModifiersAsSet().contains(PDFFontModifier.BOLD), pParagraphContext.getModifiersAsSet().contains(PDFFontModifier.ITALIC), pParagraphContext.getModifiersAsSet().contains(PDFFontModifier.SUPERSCRIPT), wordLength, notesWidth, pParagraphContext.getFontSize(), s, wordNoteReferences, isFirstWord));
				isFirstWord = false;
			}
			pParagraphContext.incCurPos();
		}
		if (!currentWords.isEmpty())
		// Some more words to process, finish the line - without justification
		{
			final PDFLine pdfLine = new PDFLine(currentWords.toArray(new PDFWord[currentWords.size()]), pParagraphContext.isCitation(), pParagraphContext.isCadre());
			addParagraphPropertiesToLine(pParagraphContext, pdfLine);
			pdfLine.setIndent(pParagraphContext.getParIndent());
			if (Justification.JUSTIFIED.equals(pParagraphContext.getJustification()))
			// Since this is the last line of the paragraph, we need a left justification instead of justified
				pdfLine.setJustification(Justification.LEFT);
			else
			// we should retain any other justification (right, centered, etc.)
				pdfLine.setJustification(pParagraphContext.getJustification());
			pdfLine.setSpaceAfter(pParagraphContext.getParagraph().getSpaceAfter());
			if (isFirstLine)
			{
				pdfLine.setSpaceBefore(pParagraphContext.getParagraph().getSpaceBefore());
				if (pParagraphContext.getParagraph().getProperties().contains(Property.LIST))
				{
					pdfLine.addLineProperty(PDFLineProperty.FIRST_OF_LIST);
					pdfLine.setIndent(pConf.getListParIndent());
				}
				else if (pParagraphContext.getParagraph().hasProperty(Property.FIRST_INDENT))
					pdfLine.setIndent((float)pParagraphContext.getParagraph().getPropertyValue(Property.FIRST_INDENT));
			}
			return pdfLine;
		}
		else
			return null;
	}

	private static PDFLine addParagraphPropertiesToLine(PDFParagraphContext pParagraphContext, PDFLine pPdfLine)
	{
		for (Property prop : pParagraphContext.getParagraph().getProperties())
		{
			switch (prop)
			{
			case CAPTION:
				pPdfLine.addLineProperty(PDFLineProperty.CAPTION);
				break;
			case CADRE:
				pPdfLine.addLineProperty(PDFLineProperty.CADRE);
				break;
			case CITATION:
				pPdfLine.addLineProperty(PDFLineProperty.CITATION);
				break;
			case TITLE1:
				pPdfLine.addLineProperty(PDFLineProperty.TITLE1);
				break;
			case TITLE2:
				pPdfLine.addLineProperty(PDFLineProperty.TITLE2);
				break;
			case TITLE3:
				pPdfLine.addLineProperty(PDFLineProperty.TITLE3);
				break;
			case TITLE4:
				pPdfLine.addLineProperty(PDFLineProperty.TITLE4);
				break;
			case TITLE5:
				pPdfLine.addLineProperty(PDFLineProperty.TITLE5);
				break;
			case FIRST_INDENT:
				if ((pPdfLine.getWords().length > 0) && (pPdfLine.getWords()[0].isFirstWordOfParagraph()))
					pPdfLine.addLineProperty(PDFLineProperty.FIRST_INDENT);
				break;

			default:
				break;
			}
		}
		return pPdfLine;
	}

	private static void log(String pString)
	{
		// Debug stuff
//		System.out.println(pString);
	}

	/**
	 * Adds (pEnding == false) or removes (pEnding == true) the modifier pModifier to the two structures.
	 * @param pModifiersAsStack
	 * @param pModifiersAsSet
	 * @param pModifier
	 * @param pEnding
	 * @throws IOException 
	 */
	private static void alterModifiers(PDFParagraphContext pParagraphContext, PDFFontModifier pModifier, boolean pEnding) throws IOException
	{
//		System.out.println("Modifier " + pModifier.name() + " " + (pEnding ? "end" : "start"));
		if (pEnding)
			if (pModifier.equals(pParagraphContext.peekModifier()))
				pParagraphContext.popModifier();
			else
				throw new IllegalStateException("Could not find incorrect modifier " + pModifier.name() + " in paragraph " + pParagraphContext.getTxt());
		else
			pParagraphContext.pushModifier(pModifier);
	}

	/**
	 * Prints the lines in the document
	 * @param pConf
	 * @param pContext
	 * @param pNotesContext
	 * @param pLine
	 * @throws IOException
	 */
	private static void printLine(final PDFConf pConf, final PDFDocumentContext pContext, final PDFNotesContext pNotesContext, final PDFLine pLine) throws IOException
	{
		final PDFWord[] words = pLine.getWords();
		// Get the maximum height of the line
		float maxPoints = 0;// make sure we get the smallest possible

		// Get all note references on this line so that we can show them later
		// Also compute the max height of the line
		final List<NoteReference> noteReferences = new ArrayList<>();
		if (!pLine.getLineProperties().contains(PDFLineProperty.IMAGE))
			for (PDFWord pdfWord : words)
			{
				// calculate the height of the line based on the tallest word
				maxPoints = Math.max(pdfWord.getPoints(), maxPoints);
				if ((pdfWord.getNoteReferences() != null) && (!pdfWord.getNoteReferences().isEmpty()))
				// we have references in this word - add them to the current page, as this might also call for a page change
					noteReferences.addAll(pdfWord.getNoteReferences());
			}
		if (maxPoints == 0)
		// Strange case where we couldn't determine the line height from the words - maybe it's an empty line
			maxPoints = pConf.getNormalFontSize();

		// move the y position to this new line's position - get ready to print it!
		if (pLine.getYPos() == -1)
			pContext.setYPos(pContext.getYPos() - pLine.getSpaceBefore() * maxPoints);
		else
			pContext.setYPos(pLine.getYPos());

		// Calculate margins
		final float outerMargin = getOuterMargin(pConf, pLine.isCitation(), pLine.isCadre());
		final float innerMargin = getInnerMargin(pConf, pLine.isCitation(), pLine.isCadre());

		final List<NoteReference> allNoteReferences = new ArrayList<>(noteReferences);
		allNoteReferences.addAll(pContext.getNotesOnPage());

		// Let's compute how much space the notes are taking at the bottom of the page to know where to stop
		float allNotesHeight = computeAllNotesHeight(pConf, pContext, allNoteReferences);
		final float lh = getLinesHeight(pConf, pContext, new PDFLine[] {pLine});
		// Check whether we need to turn a page
		if (pContext.getYPos() - (words.length == 0 ? pConf.getNormalFontSize() * pConf.getInterline() : lh) < pConf.getBottomMargin() + (pNotesContext == null ? 0f : allNotesHeight + pConf.getNotesDistanceFromText()))
		{
			log("notes-height " + allNotesHeight + " " + words[0].getText());
			// If this new line doesn't have the glued property from the last waiting line, we have to print those first
			if ((!pContext.getWaitingLines().isEmpty()) && Collections.disjoint(pLine.getLineProperties(), pContext.getWaitingLines().get(pContext.getWaitingLines().size() - 1).getGluedWith()))
			{
				final List<PDFLine> waitingLines = new ArrayList<>(pContext.getWaitingLines());
				pContext.getWaitingLines().clear();
				flushWaitingLines(pConf, pContext, pNotesContext, waitingLines);
			}

			changePDFPage(pConf, pContext, pNotesContext, true);
			if (pContext.isStopGeneration())
				return;
		}

		// Add the notes to the context *after* switching page if needed, we want these notes to be on the new page as well
		for (NoteReference noteReference : noteReferences)
			if (NoteRefType.FOOT.equals(noteReference.getType()))
				pContext.getNotesOnPage().add(noteReference);
		// Recompute height with new notes
		allNotesHeight = computeAllNotesHeight(pConf, pContext, allNoteReferences);
		if (pNotesContext != null)
		// compute the new space with the new notes
			pNotesContext.setCurrentPageBottomSpace(allNotesHeight + pConf.getNotesDistanceFromText());

		// If this new line is not glued to anything, we have to go on and print any waiting line
		if ((!pContext.getWaitingLines().isEmpty()) && ((pLine.getGluedWith() == null) || pLine.getGluedWith().isEmpty()))
		{
			final List<PDFLine> waitingLines = new ArrayList<>(pContext.getWaitingLines());
			pContext.getWaitingLines().clear();
			if (pLine.getLineProperties().contains(PDFLineProperty.CAPTION) && (waitingLines.get(waitingLines.size() - 1).getLineProperties().contains(PDFLineProperty.IMAGE)))
				waitingLines.get(waitingLines.size() - 1).setSpaceAfter(0);
			flushWaitingLines(pConf, pContext, pNotesContext, waitingLines);
		}

		// Current spacing
		float spaceWidth = words.length > 0 ? getSpacingForWord(pContext, words[words.length - 1], pConf) : 6;
		// Original X position
		float x = (pContext.getCurPageNum() % 2 == 0 ? outerMargin : innerMargin) + pLine.getIndent();
		if (!Justification.LEFT.equals(pLine.getJustification()))
		{// Not justified LEFT, there will be something to calculate here, either to center, justify or right
			float lineWidth = 0;
			int nbSpaces = 0;
			for (PDFWord word : words)
			{
				lineWidth += word.getWidth() + word.getNotesWidth();
				if (word.hasSpaceAfterWord())
				{
					nbSpaces++;
					if (Justification.CENTER.equals(pLine.getJustification()) || Justification.RIGHT.equals(pLine.getJustification()))
						lineWidth += getSpacingForWord(pContext, word, pConf);
				}
			}
			if (Justification.JUSTIFIED.equals(pLine.getJustification()))
			// Text is justified, adjust the space width so that the whole text will fit to the right margin
				spaceWidth = words.length > 1 ? (pConf.getPageWidth() - innerMargin - outerMargin - lineWidth - pLine.getIndent()) / (nbSpaces - 1) : 0;
			else if (Justification.CENTER.equals(pLine.getJustification()))
			// Text is centered, calculate where it should start
				x += (pConf.getPageWidth() - innerMargin - outerMargin - lineWidth) / 2;
			else if (Justification.RIGHT.equals(pLine.getJustification()))
			// Text is aligned to the right, calculate where it should start
				x = pConf.getPageWidth() - (pContext.getCurPageNum() % 2 == 0 ? innerMargin : outerMargin) - lineWidth;
		}
		if (pLine.getLineProperties().contains(PDFLineProperty.FIRST_OF_LIST))
		{
			final String actualText = "•";
			final float fontSize = pConf.getNormalFontSize();
			final PDFWord word = new PDFWord(pConf.getFontNameMain(), spaceWidth, fontSize, maxPoints, actualText, Collections.emptyList(), false);
			final PDFont wordFont = PDFFontFactory.getFont(pContext.getDocument(), word);

			// The first line of a list, we have to print the list bullet
			pContext.getContent().beginText();
			pContext.getContent().moveTextPositionByAmount((pContext.getCurPageNum() % 2 == 0 ? outerMargin : innerMargin) + fontSize, (pLine.getYPos() == -1 ? pContext.getYPos() : pLine.getYPos()) - getYPosOfWord(word));
			pContext.getContent().setFont(wordFont, fontSize);
			pContext.getContent().showText(actualText);
			pContext.getContent().endText();
		}
		// Store the max height of the line to go forward later
		float height = -1;
//		StringBuilder lineAsStr = new StringBuilder();
//		for (PDFWord word : words)
//			lineAsStr.append(word.getText()).append(" ");
//		System.out.println("Printing line at " + pContext.getYPos() + ": " + lineAsStr.toString());
		for (PDFWord word : words)
		{
			String wordAsStr = word.getText();
			if (!ImageType.NONE.equals(word.getImageType()))
			{
				// Deal with images
				float h = getPDFWordYSpacing(word, pConf);
				final float y = (pLine.getYPos() == -1 ? pContext.getYPos() : pLine.getYPos()) - h;
				if (ImageType.FULL_WIDTH.equals(word.getImageType()))
				// Because when images are on a single line, we don't want an indentation for this specific case
					x -= pLine.getIndent();
				if (pLine.getGluedWith() == null)
					pContext.getContent().drawXObject(PDImageXObject.createFromFile(pContext.getSourceDir().getAbsolutePath() + "/" + wordAsStr, pContext.getDocument()), x, y, getSpacingForWord(pContext, word, pConf)/*pConf.getPageWidth() - pConf.getInnerMargin() - pConf.getOuterMargin()*/, h);
				if (height < h)
					height = h;
			}
			else
			{
				// Deal with words
				// special cases when we have separators after the word, isolate them
				final float y = (pLine.getYPos() == -1 ? pContext.getYPos() : pLine.getYPos());
				final List<Integer> extraSepsCP = new ArrayList<>();
				final List<Integer> wordCPs = new ArrayList<>(wordAsStr.codePoints().boxed().collect(Collectors.toList()));
				while ((wordCPs.size() > 0) && SEPARATORS.contains(wordCPs.get(wordCPs.size() - 1)))
					extraSepsCP.add(0, wordCPs.remove(wordCPs.size() - 1));
				final String actualText = extraSepsCP.isEmpty() ? wordAsStr : new String(wordCPs.stream().mapToInt(i -> i).toArray(), 0, wordCPs.size());
				final PDFont wordFont = PDFFontFactory.getFont(pContext.getDocument(), word);
				final float fontSize = PDFFontFactory.getSize(word);
				if (pLine.getGluedWith() == null)
				{
					pContext.getContent().beginText();
					pContext.getContent().moveTextPositionByAmount(x, y - getYPosOfWord(word));
					pContext.getContent().setFont(wordFont, fontSize);
//					pContext.getContent().setCharacterSpacing(-0.3f);
					pContext.getContent().showText(actualText);
					pContext.getContent().endText();
				}
				if (extraSepsCP.isEmpty())
					x += word.getWidth();
				else
				// we need to recompute the width of the word without its separators
					x += (wordFont.getStringWidth(actualText) / 1000) * fontSize;

				float h = (float)((PDFFontFactory.getFont(pContext.getDocument(), word).getFontDescriptor().getCapHeight() / 1000 * word.getPoints() * pConf.getInterline()) + pLine.getSpaceAfter() * maxPoints);
				if (h > height)
					height = h;
				if (!word.getNoteReferences().isEmpty())
				{
					// Now that we are integrating the word, we do have to integrate its references as well
					for (NoteReference noteReference : word.getNoteReferences())
						switch (noteReference.getType())
						{
						case FOOT:
							pNotesContext.getFootNotes().add(noteReference);
							break;
						case CHAPTER:
							pNotesContext.getChapNotes().add(noteReference);
							break;
						case GLOBAL:
							pNotesContext.getGlobNotes().add(noteReference);
							break;
						default:
							throw new IllegalArgumentException("Unexpected value: " + noteReference.getType());
						}
					final float noteRefPoints = PDFFontFactory.getSize(word.getPoints(), true);
					PDFont noteFont = PDFFontFactory.getFont(pContext.getDocument(), pConf.getFontNameNotes(), false, false);
					final List<String> notesStrList = word.getNoteReferences().stream()
							.map(d -> d.getRefId())
							.collect(Collectors.toList());
					final String notesStr = String.join(",", notesStrList);
					if (pLine.getGluedWith() == null)
					{
						pContext.getContent().beginText();
						pContext.getContent().moveTextPositionByAmount(x, y - noteRefPoints * 1.3f);
						pContext.getContent().setFont(noteFont, noteRefPoints);
						pContext.getContent().showText(notesStr);
						pContext.getContent().endText();
					}
					x += word.getNotesWidth();
				}
				if (!extraSepsCP.isEmpty())
				{
					// now show the extra separators
					final String separatorsText = new String(extraSepsCP.stream().mapToInt(i -> i).toArray(), 0, extraSepsCP.size());
					if (pLine.getGluedWith() == null)
					{
						pContext.getContent().beginText();
						pContext.getContent().moveTextPositionByAmount(x, y - getYPosOfWord(word));
						pContext.getContent().setFont(wordFont, fontSize);
						pContext.getContent().showText(separatorsText);
						pContext.getContent().endText();
					}
					x += (wordFont.getStringWidth(separatorsText) / 1000) * fontSize;
				}
				if (word.hasSpaceAfterWord())
					x += spaceWidth;
			}
		}
		if (pLine.getGluedWith() != null)
		{
			pLine.setYPos(pContext.getYPos());
			pContext.getWaitingLines().add(pLine);
		}

		if (height == -1)
		// weird, take the default space - this might have been an empty line
			height = (float)((pConf.getNormalFontSize() * pConf.getInterline() * pConf.getInterline()));
//		if (pLine.getGluedWith() == null)
		// move the cursor down to get ready for the next line
		pContext.setYPos((float)(pContext.getYPos() - height - pLine.getSpaceAfter() * maxPoints));
	}

	private static float computeAllNotesHeight(final PDFConf pConf, final PDFDocumentContext pContext, final List<NoteReference> pNoteReferences) throws IOException
	{
		float allNotesHeight = 0;
		// compute the max width of the biggest reference indicator
		final float biggestNoteIndicatorWidth = computeMaxNoteIndicatorWidth(pConf, pContext, pNoteReferences);
//		System.out.println("--");
		for (NoteReference noteReference : pNoteReferences)
		{
			if (allNotesHeight > 0)
				allNotesHeight += pConf.getNoteNoteSpacing();// add space between notes
//			else
//			// separation between normal text and the notes
//				allNotesHeight += pConf.getNotesDistanceFromText();
			final Map<String, FMNote> notes = pContext.getFMDocument().getNotes();
			final List<FMParagraph> paragraphs = (notes == null) || (!notes.containsKey(noteReference.getNoteId())) ? new ArrayList<>() : notes.get(noteReference.getNoteId()).getContents();
			final PDFNotesContext notesContext = new PDFNotesContext(pConf.getBottomMargin());
			for (FMParagraph paragraph : paragraphs)
			{
				boolean firstLine = true;
				PDFLine noteLine;
				final PDFParagraphContext paragraphContext = new PDFParagraphContext(paragraph, pConf.getFontNameNotes(), pConf.getNoteFontSize());
				paragraphContext.setParIndent(biggestNoteIndicatorWidth);
				while ((noteLine = getNextLine(pConf, pContext, paragraphContext, notesContext)) != null)
				{
					noteLine.setIndent(biggestNoteIndicatorWidth);
					float maxLineHeight = 0;
					for (PDFWord noteWord : noteLine.getWords())
						maxLineHeight = Math.max(maxLineHeight, noteWord.getPoints());
					allNotesHeight += maxLineHeight * (firstLine ? 1f : pConf.getNoteInterline()) + noteLine.getSpaceBefore() + noteLine.getSpaceAfter();
					firstLine = false;
				}
			}
			allNotesHeight += pConf.getNoteInterline() * 0.3 * (paragraphs.size() - 0) * pConf.getNoteFontSize();
			log("After " + noteReference.getNoteId() + ": " + allNotesHeight + " (bottom is " + pConf.getBottomMargin() + ")");
		}
		return allNotesHeight;
	}

	private static float getYPosOfWord(PDFWord pWord)
	{
		return pWord.isSuperscript() ? PDFFontFactory.getSize(pWord) : PDFFontFactory.getSize(pWord);
	}

	public static float getInnerMargin(PDFConf pConf, boolean pCitation, boolean pCadre)
	{
		return pConf.getInnerMargin() + (pCitation ? Math.max(pConf.getOuterMargin(), pConf.getInnerMargin()) * 1 : 0) + (pCadre ? 5f : 0);
	}

	public static float getOuterMargin(PDFConf pConf, boolean pCitation, boolean pCadre)
	{
		return pConf.getOuterMargin() + (pCitation ? Math.max(pConf.getOuterMargin(), pConf.getInnerMargin()) * 1 : 0) + (pCadre ? 5f : 0);
	}

	public static float getLeftMargin(PDFConf pConf, PDFDocumentContext pContext, boolean pCitation, boolean pCadre)
	{
		return pContext.getCurPageNum() % 2 != 0 ? getInnerMargin(pConf, pCitation, pCadre) : getOuterMargin(pConf, pCitation, pCadre);
	}

	public static float getRightMargin(PDFConf pConf, PDFDocumentContext pContext, boolean pCitation, boolean pCadre)
	{
		return pConf.getPageWidth() - (pContext.getCurPageNum() % 2 != 0 ? getOuterMargin(pConf, pCitation, pCadre) : getInnerMargin(pConf, pCitation, pCadre));
	}
}
