package jyt.freemind.converter.pdf;

/**
 * Different fonts used in the document
 * @author jytou
 */
public interface PDFFont
{
	public static final String MAIN = "FontNameMain";
	public static final String TITLES = "FontNameTitles";
	public static final String CAPTIONS = "FontNameCaptions";
	public static final String CITATIONS = "FontNameCitations";
	public static final String NOTES = "FontNameNotes";
	public static final String HEADERS = "FontNameHeaders";
}
