package jyt.freemind.converter.pdf;

import java.util.HashMap;
import java.util.Map;

/**
 * The custom configuration for the PDF output: page and font sizes, etc.
 * @author jytou
 */
public class PDFConf
{
	private float mPageWidth;
	private float mPageHeight;
	private float mTopMargin;
	private float mBottomMargin;
	private float mInnerMargin;
	private float mOuterMargin;
	private float mNormalFontSize;
	private float mHeadersFontSize;
	private String mFontNameMain;
	private String mFontNameTitles;
	private String mFontNameCaptions;
	private String mFontNameCitations;
	private String mFontNameHeaders;
	private String mFontNameNotes;
	// font size for notes
	private int mNoteFontSize;
	// interline between two lines in the same notes paragraph
	private float mNoteInterline;
	// distance between two notes on the same page
	private float mNoteNoteSpacing;
	// the minimum distance between the regular text on the page and the first note
	private float mNotesDistanceFromText;
	private float mInterline;
	private float mExtraInterPar;
	private int[] mTitleFontSizes;
	private boolean mChaptersStartOnEven;
	private Map<String, String> mMetas;
	private int mMaxPages;

	private PDFConf()
	{
		super();
	}

	public PDFConf(
			float pPageWidth,
			float pPageHeight,
			float pTopMargin,
			float pBottomMargin,
			float pInnerMargin,
			float pOuterMargin,
			float pNormalFontSize,
			float pInterlines,
			float pExtraInterPar,
			int[] pTitleFontSizes,
			Map<String, String> pMetas,
			boolean pChaptersStartOnEven,
			int pMaxPages,
			int pNoteFontSize,
			float pNotesNoteSpacing,
			float pNotesInterline,
			float pNotesDistanceFromText)
	{
		super();
		mPageWidth = convert(pPageWidth);
		mPageHeight = convert(pPageHeight);
		mTopMargin = convert(pTopMargin);
		mBottomMargin = convert(pBottomMargin);
		mInnerMargin = convert(pInnerMargin);
		mOuterMargin = convert(pOuterMargin);
		mNormalFontSize = pNormalFontSize;
		mInterline = pInterlines;// this is a proportion of the normal line height
		mExtraInterPar = pExtraInterPar;// another proportion
		mTitleFontSizes = pTitleFontSizes;
		mMetas = pMetas;
		mChaptersStartOnEven = pChaptersStartOnEven;
		mMaxPages = pMaxPages;
		mNoteFontSize = pNoteFontSize;
		mNoteNoteSpacing = convert(pNotesNoteSpacing);
		mNoteInterline = pNotesInterline;// proportion
		mNotesDistanceFromText = convert(pNotesDistanceFromText);
	}

	public PDFConf clone()
	{
		final PDFConf c = new PDFConf();
		c.mBottomMargin = mBottomMargin;
		c.mChaptersStartOnEven = mChaptersStartOnEven;
		c.mExtraInterPar = mExtraInterPar;
		c.mFontNameCaptions = mFontNameCaptions;
		c.mFontNameCitations = mFontNameCitations;
		c.mFontNameMain = mFontNameMain;
		c.mFontNameNotes = mFontNameNotes;
		c.mFontNameTitles = mFontNameTitles;
		c.mFontNameHeaders = mFontNameHeaders;
		c.mInnerMargin = mInnerMargin;
		c.mInterline = mInterline;
		c.mMaxPages = mMaxPages;
		c.mMetas = new HashMap<>(mMetas);
		c.mNormalFontSize = mNormalFontSize;
		c.mNoteFontSize = mNoteFontSize;
		c.mHeadersFontSize = mHeadersFontSize;
		c.mNoteInterline = mNoteInterline;
		c.mNoteNoteSpacing = mNoteNoteSpacing;
		c.mNotesDistanceFromText = mNotesDistanceFromText;
		c.mOuterMargin = mOuterMargin;
		c.mPageHeight = mPageHeight;
		c.mPageWidth = mPageWidth;
		c.mTitleFontSizes = mTitleFontSizes;
		c.mTopMargin = mTopMargin;
		return c;
	}

	/**
	 * Converts a value in mm (entered by the user) into points (which is what a PDF waits for).
	 * @param pInMM
	 * @return
	 */
	private float convert(float pInMM)
	{
		return (float)(72.0 * pInMM / 25.4);
	}

	public Map<String, String> getMetas()
	{
		return mMetas;
	}

	public float getPageWidth()
	{
		return mPageWidth;
	}
	public float getPageHeight()
	{
		return mPageHeight;
	}
	public float getTopMargin()
	{
		return mTopMargin;
	}
	public float getBottomMargin()
	{
		return mBottomMargin;
	}
	public void setBottomMargin(float pBottomMargin)
	{
		mBottomMargin = pBottomMargin;
	}
	public float getInnerMargin()
	{
		return mInnerMargin;
	}
	public float getOuterMargin()
	{
		return mOuterMargin;
	}
	public float getHeadersFontSize()
	{
		return mHeadersFontSize;
	}
	public PDFConf setHeadersFontSize(float pHeadersFontSize)
	{
		mHeadersFontSize = pHeadersFontSize;
		return this;
	}
	public float getNormalFontSize()
	{
		return mNormalFontSize;
	}
	public PDFConf setNormalFontSize(float pNormalFontSize)
	{
		mNormalFontSize = pNormalFontSize;
		return this;
	}
	public int[] getTitleFontSizes()
	{
		return mTitleFontSizes;
	}
	public float getInterline()
	{
		return mInterline;
	}
	public PDFConf setInterline(float pInterline)
	{
		mInterline = pInterline;
		return this;
	}
	public float getExtraInterPar()
	{
		return mExtraInterPar;
	}
	public PDFConf setExtraInterPar(float pExtraInterPar)
	{
		mExtraInterPar = pExtraInterPar;
		return this;
	}
	public boolean isChaptersStartOnEven()
	{
		return mChaptersStartOnEven;
	}
	public int getMaxPages()
	{
		return mMaxPages;
	}

	public String getFontNameMain()
	{
		return mFontNameMain;
	}

	public PDFConf setFontNameMain(String pFontNameMain)
	{
		mFontNameMain = pFontNameMain;
		return this;
	}

	public String getFontNameTitles()
	{
		return mFontNameTitles;
	}

	public PDFConf setFontNameTitles(String pFontNameTitles)
	{
		mFontNameTitles = pFontNameTitles;
		return this;
	}

	public String getFontNameCaptions()
	{
		return mFontNameCaptions;
	}

	public PDFConf setFontNameCaptions(String pFontNameCaptions)
	{
		mFontNameCaptions = pFontNameCaptions;
		return this;
	}

	public String getFontNameCitations()
	{
		return mFontNameCitations;
	}

	public PDFConf setFontNameCitations(String pFontNameCitations)
	{
		mFontNameCitations = pFontNameCitations;
		return this;
	}

	public String getFontNameHeaders()
	{
		return mFontNameHeaders;
	}

	public PDFConf setFontNameHeaders(String pFontNameHeaders)
	{
		mFontNameHeaders = pFontNameHeaders;
		return this;
	}

	public String getFontNameNotes()
	{
		return mFontNameNotes;
	}

	public PDFConf setFontNameNotes(String pFontNameNotes)
	{
		mFontNameNotes = pFontNameNotes;
		return this;
	}

	public int getNoteFontSize()
	{
		return mNoteFontSize;
	}

	public float getNoteInterline()
	{
		return mNoteInterline;
	}

	public float getNoteNoteSpacing()
	{
		return mNoteNoteSpacing;
	}

	public float getNotesDistanceFromText()
	{
		return mNotesDistanceFromText;
	}

	public void setNotesDistanceFromText(float pNotesDistanceFromText)
	{
		mNotesDistanceFromText = pNotesDistanceFromText;
	}

	public float getListParIndent()
	{
		return mNormalFontSize * 2;
	}
}
