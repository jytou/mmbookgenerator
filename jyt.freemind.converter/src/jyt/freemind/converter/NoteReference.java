package jyt.freemind.converter;

public class NoteReference
{
	public enum NoteRefType {FOOT, CHAPTER, GLOBAL};

	// identifier of the reference, for instance an integer
	private String mRefId;
	// referenced note
	private String mNoteId;
	// type
	private NoteRefType mType;

	public NoteReference(String pRefId, String pNoteId, NoteRefType pType)
	{
		super();
		mRefId = pRefId;
		mNoteId = pNoteId;
		mType = pType;
	}

	public String getRefId()
	{
		return mRefId;
	}

	public String getNoteId()
	{
		return mNoteId;
	}

	public NoteRefType getType()
	{
		return mType;
	}
}
