package jyt.freemind.converter.rtf;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;

import com.itseasy.rtf.RTFDocument;
import com.itseasy.rtf.text.Font;
import com.itseasy.rtf.text.InfoGroup;
import com.itseasy.rtf.text.Paragraph;
import com.itseasy.rtf.text.TextPart;

import jyt.freemind.converter.ConvUtil;
import jyt.freemind.converter.FMNode;
import jyt.freemind.converter.FMParagraph;

/**
 * Builds a basic RTF document
 * @author jytou
 */
public class RTFBuilder
{
	public final static int[] mLevelSizes = new int[] {16, 14, 12, 10};
	public static int sChapNumRTF = 1;

	public static void buildRTFFile(final FMNode pFmNode, final Map<String, String> pMeta, String pFileName) throws IOException
	{
		RTFDocument doc = new RTFDocument();
		String author = pMeta.get(InfoGroup.INFO_AUTHOR);
		if (author == null)
			author =  "Jean-Yves Toumit";
		doc.getInfo().setInfoAsString(InfoGroup.INFO_AUTHOR, author);
		if (pMeta.containsKey(InfoGroup.INFO_TITLE))
			doc.getInfo().setInfoAsString(InfoGroup.INFO_TITLE, pMeta.get(InfoGroup.INFO_TITLE));
		fillDocument(doc, pFmNode);
		doc.save(new File(pFileName));
	}

	private static void fillDocument(RTFDocument pDoc, FMNode pNode)
	{
		String label = pNode.getLabel();
		if (!label.isEmpty())
		{
			final int nodeLevel = ConvUtil.getNodeLevel(pNode);
			final float fontSize = nodeLevel >= mLevelSizes.length ? 16 : mLevelSizes[nodeLevel];
			if (label.startsWith("#"))
				label = "Chapitre " + String.valueOf(sChapNumRTF++) + ". " + label.substring(1);
			Paragraph header = new Paragraph(10, 7, (int)fontSize, Font.ARIAL, new TextPart(label));
			header.setAlignment(Paragraph.ALIGN_CENTER);
			pDoc.addParagraph(header);
		}
		if (pNode.getParagraphs() != null)
		{
			for (FMParagraph para : pNode.getParagraphs())
			{
				Paragraph par = new Paragraph(5, 5, mLevelSizes[mLevelSizes.length - 1], Font.TIMES_NEW_ROMAN, new TextPart(StringEscapeUtils.unescapeHtml4(para.getText())));
				pDoc.addParagraph(par);
			}
		}
		for (FMNode node : pNode.getChildren())
			fillDocument(pDoc, node);
	}

}
