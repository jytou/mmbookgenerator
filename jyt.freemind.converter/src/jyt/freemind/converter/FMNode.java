package jyt.freemind.converter;

import java.util.ArrayList;
import java.util.List;

import jyt.freemind.converter.FMParagraph.Property;

/**
 * A node from a FreeMind file.
 * @author jytou
 */
public class FMNode
{
	// The parent node
	private FMNode mParent;
	// The children of this node
	private List<FMNode> mChildren = new ArrayList<FMNode>();
	// This node's label
	private String mLabel = null;
	// The paragraphs in this node
	private List<FMParagraph> mParagraphs = null;
	// The size (in characters) of this node
	private int mSize = 0;
	// The number of words in this node
	private int mWords = 0;
	// the first page for this node
	private int mStartPage = -1;

	public FMNode(FMNode pParent, String pLabel)
	{
		super();
		mParent = pParent;
		mLabel = pLabel;
		if (mParent != null)
			mParent.getChildren().add(this);
	}

	public FMNode cloneTree()
	{
		return cloneTree(null, this);
	}

	public static FMNode cloneTree(FMNode pDupParent, FMNode pNode)
	{
		final FMNode clone = new FMNode(pDupParent, pNode.mLabel);
		clone.mSize = pNode.mSize;
		clone.mWords = pNode.mWords;
		if (pNode.getParagraphs() != null)
		{
			clone.mParagraphs = new ArrayList<>();
			for (FMParagraph paragraph : pNode.mParagraphs)
			{
				final FMParagraph clonedParagraph = new FMParagraph(paragraph.getText(), paragraph.getSpaceBefore(), paragraph.getSpaceAfter());
				for (Property property : paragraph.getProperties())
					clonedParagraph.setProperty(property, paragraph.getPropertyValue(property));
				clone.mParagraphs.add(clonedParagraph);
			}
		}
		for (FMNode child : pNode.getChildren())
			cloneTree(clone, child);
		return clone;
	}

	public FMNode getParent()
	{
		return mParent;
	}

	public List<FMNode> getChildren()
	{
		return mChildren;
	}

	public String getLabel()
	{
		return mLabel;
	}

	public List<FMParagraph> getParagraphs()
	{
		return mParagraphs;
	}

	public void addParagraph(FMParagraph pParagraph)
	{
		if (mParagraphs == null)
			mParagraphs = new ArrayList<FMParagraph>();
		mParagraphs.add(pParagraph);
	}

	/**
	 * Computes this node's size and number of words.
	 */
	public void computeSize()
	{
		mSize = 0;
		mWords = 0;
		if (mParagraphs != null)
			for (FMParagraph paragraph : mParagraphs)
			{
				mSize += paragraph.getText().length();
				mWords += paragraph.getText().split("\\s+").length;
			}
		if (mChildren != null)
			for (FMNode fmNode : mChildren)
			{
				fmNode.computeSize();
				mSize += fmNode.mSize;
				mWords += fmNode.mWords;
			}
	}

	public int getSize()
	{
		return mSize;
	}

	public int getWords()
	{
		return mWords;
	}

	public int getStartPage()
	{
		return mStartPage;
	}

	public void setStartPage(int pStartPage)
	{
		mStartPage = pStartPage;
	}
}
