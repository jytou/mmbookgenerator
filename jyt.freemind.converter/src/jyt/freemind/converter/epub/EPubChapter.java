package jyt.freemind.converter.epub;

import java.util.ArrayList;
import java.util.List;

import jyt.freemind.converter.FMNode;

/**
 * One Chapter in an EPUB file. It consist of a root node and the different chapter numbers
 * @author jytou
 */
public class EPubChapter
{
	// The hierarchy of chapter numbers/identificators (can be letters or others.)
	private String[] mChapter;
	// Chapter ID to be used in the toc
	private int mChapId;
	// The root node associated with this chapter
	private FMNode mNode;
	// The number of the file in which this (sub)chapter is stored. Useful to create links to an anchor in that (sub)chapter
	private int mChapFileNum;

	private List<EPubChapter> mChildren = new ArrayList<EPubChapter>();

	public EPubChapter(String[] pChapter, int pChapId, FMNode pNode, int pChapFileNum)
	{
		super();
		mChapter = pChapter;
		mChapId = pChapId;
		mNode = pNode;
		mChapFileNum = pChapFileNum;
	}

	public int getChapId()
	{
		return mChapId;
	}

	public int getChapFileNum()
	{
		return mChapFileNum;
	}

	public String[] getChapter()
	{
		return mChapter;
	}

	public FMNode getNode()
	{
		return mNode;
	}

	public String getTitle()
	{
		// Used in the templates
		return mNode.getLabel();
	}

	public List<EPubChapter> getChildren()
	{
		return mChildren;
	}

	public String getComputedFileName()
	{
		return new StringBuilder("chap").append(mChapId).toString();
	}
}
