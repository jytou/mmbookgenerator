package jyt.freemind.converter.epub;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import jyt.freemind.converter.ConvUtil;
import jyt.freemind.converter.ConvertMM;
import jyt.freemind.converter.FMDocument;
import jyt.freemind.converter.FMNode;
import jyt.freemind.converter.FMParagraph;
import jyt.freemind.converter.FMParagraph.Property;
import jyt.freemind.converter.NoteReference;

/**
 * Builds an epub for a book.
 * It builds the different files from templates using Velocity and then zips the file into a unique epub.
 * @author jytou
 */
public class EpubBuilder
{
	// Where to fetch templates
	static final String RES_ROOT = "/jyt/freemind/converter/epub/";
	static final Map<ChapNumType, String[]> CHAP_NUM_TYPES = new HashMap<ChapNumType, String[]>();
	enum ChapNumType {NUMBER, ALPHAL, ALPHAU, ROMANL, ROMANU}

	private static List<NoteReference> sNoteReferences = new ArrayList<>();
	static
	{
		for (ChapNumType chapNumType : ChapNumType.values())
			CHAP_NUM_TYPES.put(chapNumType, buildChapNums(chapNumType));
	}

	/**
	 * Builds the epub file.
	 * @param pDocument 
	 * @param pSourceDir
	 * @param pRootNode
	 * @param pMeta
	 * @param pFileName
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws Exception
	 */
	public static void buildEpubFile(FMDocument pDocument, final File pSourceDir, final FMNode pRootNode, final Map<String, String> pMeta, String pFileName) throws IOException, URISyntaxException, Exception
	{
		sNoteReferences.clear();

		// Create the epub chapters in memory from the nodes
		final List<EPubChapter> chapters = new ArrayList<>();
		buildEPubChapters(pRootNode, chapters);

		// Create the base directory and mandatory files
		final Path tmp = Files.createTempDirectory("myepub");
		System.out.println(tmp.toString());
		Files.copy(Paths.get(ConvertMM.class.getResource(RES_ROOT + "mimetype").toURI()), Paths.get(tmp.toString(), "mimetype"));
		Files.copy(Paths.get(ConvertMM.class.getResource(RES_ROOT + "page_styles.css").toURI()), Paths.get(tmp.toString(), "page_styles.css"));
		Files.copy(Paths.get(ConvertMM.class.getResource(RES_ROOT + "stylesheet.css").toURI()), Paths.get(tmp.toString(), "stylesheet.css"));
		Files.copy(Paths.get(ConvertMM.class.getResource(RES_ROOT + "cover.xhtml").toURI()), Paths.get(tmp.toString(), "cover.xhtml"));
		Files.createDirectory(Paths.get(tmp.toFile().getAbsolutePath(), "META-INF"));
		Files.copy(Paths.get(ConvertMM.class.getResource(RES_ROOT + "container.xml").toURI()), Paths.get(tmp.toString(), "META-INF/container.xml"));

		// Init Velocity
		final VelocityEngine ve = new VelocityEngine();
		ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		ve.init();

		// Create chapter files
		for (EPubChapter chapter : chapters)
		{
			final VelocityContext context = new VelocityContext();
			context.put("contents", getHtmlNodeContents(pDocument, chapter, pMeta));
			context.put("chapterTitle", chapter.getNode().getLabel());
			try (final FileWriter w = new FileWriter(tmp + "/" + chapter.getComputedFileName() + ".xhtml"))
			{
				ve.mergeTemplate(RES_ROOT + "chapter.xhtml", "UTF-8", context, w);
			}
		}

		// Create TOC
		VelocityContext context = new VelocityContext();
		context.put("uuid", pMeta.get("UUID"));
		context.put("title", pMeta.get("Title"));
		final List<EPubChapterTOC> tocChapters = new ArrayList<>();
		for (EPubChapter chapter : chapters)
		{
			final String strOrder = String.format("%03d", chapter.getChapId());
			String label = chapter.getNode().getLabel();
			if (label.trim().isEmpty())
				label = "chap" + chapter.getChapId();
			tocChapters.add(new EPubChapterTOC("n" + strOrder, String.valueOf(chapter.getChapId()), label, chapter.getComputedFileName() + ".xhtml"));
		}
		context.put("chapters", tocChapters);

		// Write the title file
		context.put("author", pMeta.get("Author"));
		try (final FileWriter w = new FileWriter(tmp + "/titlepage.xhtml"))
		{
			ve.mergeTemplate(RES_ROOT + "titlepage.xhtml", "UTF-8", context, w);
		}

		// Write the resulting TOC file
		try (final FileWriter w = new FileWriter(tmp + "/toc.ncx"))
		{
			ve.mergeTemplate(RES_ROOT + "toc.ncx", "UTF-8", context, w);
		}

		if (!sNoteReferences.isEmpty())
		{
			Files.copy(Paths.get(ConvertMM.class.getResource(RES_ROOT + "notes.xhtml").toURI()), Paths.get(tmp.toString(), "notes.xhtml"));
			try (final FileWriter w = new FileWriter(tmp + "/notes.xhtml"))
			{
				final VelocityContext notesContext = new VelocityContext();
				notesContext.put("notes", sNoteReferences);
				ve.mergeTemplate(RES_ROOT + "notes.xhtml", "UTF-8", notesContext, w);
			}
		}

		// Copy the cover and the different images used in the document in the same directory structure
		final List<EPubImage> epubImages = new ArrayList<>();
		Files.copy(Paths.get(pSourceDir.getAbsolutePath(), "cover.jpg"), Paths.get(tmp.toString(), "cover.jpg"));
		final File imgDir = new File(pSourceDir, "images");
		if (imgDir.exists())
		{
			new File(Paths.get(tmp.toString(), "images").toString()).mkdirs();
			for (File img : imgDir.listFiles())
			{
				final String src = "images/" + img.getName();
				String ext = img.getName().substring(img.getName().lastIndexOf('.') + 1);
				if ("jpg".equals(ext))
					ext = "jpeg";// because the MIME type jpeg is not jpg...
				epubImages.add(new EPubImage(src, img.getName(), ext));
				Files.copy(img.toPath(), Paths.get(tmp.toString() + "/images", img.getName()));
			}
		}

		// Create OPF
		context = new VelocityContext();
		context.put("language", pMeta.get("Language"));
		context.put("title", pMeta.get("Title"));
		context.put("creatorAs", pMeta.get("Author"));
		context.put("creatorFull", pMeta.get("Author"));
		context.put("publisher", pMeta.get("Publisher"));
		context.put("dateTZ", pMeta.get("DateTZ"));
		context.put("uuid", pMeta.get("UUID"));
		context.put("chapters", tocChapters);
		context.put("images", epubImages);
		if (!sNoteReferences.isEmpty())
			context.put("notes", sNoteReferences);
		try (final FileWriter w = new FileWriter(tmp + "/content.opf"))
		{
			ve.mergeTemplate(RES_ROOT + "content.opf", "UTF-8", context, w);
		}

		// Create the final epub file
		ZipFolder.zipFolder(tmp, Paths.get(pFileName));
	}

	private static String[] buildChapNums(ChapNumType pChapNumType)
	{
		final List<String> nums = new ArrayList<>();
		for (int i = 0; i < 26; i++)
		{
			switch (pChapNumType)
			{
			case NUMBER:
				nums.add(String.valueOf(i+1));
				break;
			case ALPHAL:
				nums.add("" + (char)('a' + i));
				break;
			case ALPHAU:
				nums.add("" + (char)('A' + i));
				break;
			case ROMANL:
				nums.add(toRoman(i+1, false));
				break;
			case ROMANU:
				nums.add(toRoman(i+1, true));
				break;

			default:
				break;
			}
		}
		return nums.toArray(new String[nums.size()]);
	}

	public static String toRoman(int pNum, boolean pUpper)
	{
		final StringBuilder sb = new StringBuilder();
		while (pNum > 10)
		{
			sb.append(pUpper ? 'X' : 'x');
			pNum -= 10;
		}
		if (pNum == 9)
			sb.append(pUpper ? "IX" : "ix");
		else
		{
			if (pNum >= 5)
			{
				sb.append(pUpper ? 'V' : 'v');
				pNum -= 5;
			}
			if (pNum == 4)
				sb.append(pUpper ? "IV" : "iv");
			else
			{
				while (pNum > 0)
				{
					sb.append(pUpper ? 'I' : 'i');
					pNum -= 1;
				}
			}
		}
		return sb.toString();
	}

	/**
	 * Retrieves the HTML contents of a node.
	 * @param pDocument 
	 * @param pNode
	 * @param pMeta
	 * @param pChapNum
	 * @return
	 */
	static String getHtmlNodeContents(FMDocument pDocument, EPubChapter pNode, Map<String, String> pMeta)
	{
		final StringBuilder sb = new StringBuilder();
		getHtmlNodeContents(pDocument, pNode, sb, pMeta);
		return sb.toString();
	}

	/**
	 * Recursively generates the contents for a node and its subnodes.
	 * @param pDocument 
	 * @param pNode
	 * @param pSb
	 * @param pMeta
	 * @param pSubChapter
	 */
	private static void getHtmlNodeContents(FMDocument pDocument, EPubChapter pNode, StringBuilder pSb, Map<String, String> pMeta)
	{
		final String idAttr = " id='n" + String.format("%03d", pNode.getChapId()) + "'";
		String label = pNode.getNode().getLabel();
		if (label.startsWith("##"))
			label = label.substring(0, 1) + label.substring(2);// deal with this old convention
		final String thisChapNum = pNode.getChapter()[pNode.getChapter().length - 1];
		if (thisChapNum != null)
		{
			final StringBuilder sb = new StringBuilder();
			for (String chapNum : pNode.getChapter())
			{
				if (sb.length() > 0)
					sb.append('.');
				sb.append(chapNum);
			}
			label = (pNode.getChapter().length == 1 ? MessageFormat.format(ConvertMM.sLangProps.getProperty("chapter"), sb.toString(), label.substring(1)) : label.substring(1));
		}
		final String title = "<h" + pNode.getChapter().length + idAttr + ">" + label + "</h" + pNode.getChapter().length + ">\n";
		pSb.append(title).append("\n");
		if (pNode.getNode().getParagraphs() != null)
		{
			for (final FMParagraph para : pNode.getNode().getParagraphs())
			{
				if (para.getText().contains(ConvUtil.ISBN_TAG))
				{
					if (pMeta.containsKey(ConvUtil.KEY_ISBN))
					{
						String isbn = pMeta.get(ConvUtil.KEY_ISBN);
						if ((isbn != null) && (!isbn.isEmpty()))
							para.setText(para.getText().replace(ConvUtil.ISBN_TAG, isbn));
						else
							continue;
					}
					else
						continue;
				}
				pSb.append("<p");
				if (!para.getProperties().isEmpty())
				{
					pSb.append(" style='");
					boolean first = true;
					for (Property prop : para.getProperties())
					{
						if (prop.getRepresentation().isEmpty() || para.getPropertyValue(prop).toString().isEmpty())
							continue;
						if (first)
							first = false;
						else
							pSb.append(", ");
						pSb.append(prop.getRepresentation()).append(": ").append(para.getPropertyValue(prop));
					}
					pSb.append("'");
				}
				pSb.append(">\n");
				String txt = para.getText();
				txt = txt.replaceAll("<(/)?font[^>]*>", "");
				do
				{
			        final Pattern pattern = Pattern.compile("\\{(fn|cn|gn|foot-note|chap-note|glob-note)\\:[a-zA-Z0-9\\-_]+\\}");
			        final Matcher matcher = pattern.matcher(txt);
					if (matcher.find())
					{
						final int pos = matcher.start();
						final String found = matcher.group();
						final String referenceName = found.substring(found.indexOf(':') + 1, found.indexOf('}'));
						final StringBuilder noteContents = new StringBuilder();
						boolean first = true;
						for (FMParagraph fmParagraph : pDocument.getNotes().get(referenceName).getContents())
						{
							if (first)
								first = false;
							else
								noteContents.append("<br/>");
							noteContents.append(fmParagraph.getText());
						}
						final EpubNoteReference noteReference = new EpubNoteReference("" + (sNoteReferences.size() + 1), referenceName, "chap" + pNode.getChapFileNum() + ".xhtml#ref-" + referenceName, noteContents.toString());
						sNoteReferences.add(noteReference);
						final String noteLink = "<sup><a class=\"reference\" id=\"ref-" + referenceName + "\" href=\"notes.xhtml#note-" + referenceName + "\">" + noteReference.getRefId() + "</a></sup>";
						txt = txt.substring(0, pos) + noteLink + txt.substring(pos + found.length());
					}
					else
						break;
				}
				while (true);
				pSb.append(txt);
				pSb.append("\n</p>\n");
			}
		}
		for (final EPubChapter chap : pNode.getChildren())
			getHtmlNodeContents(pDocument, chap, pSb, pMeta);
	}

	/**
	 * Creates chapters from the original nodes.
	 * @param pRootNode
	 * @param pChapters
	 * @param pParents
	 */
	static void buildEPubChapters(FMNode pRootNode, List<EPubChapter> pChapters)
	{
		buildEPubChapters(pRootNode, pChapters, new Stack<>());
	}
	/**
	 * Creates chapters from the original nodes.
	 * @param pNode
	 * @param pChapters
	 * @param pParents
	 */
	static void buildEPubChapters(FMNode pNode, List<EPubChapter> pChapters, Stack<EPubChapter> pParents)
	{
		int chapNum = 0;
		int idOrder = 1;
		final EPubChapter parent = pParents.isEmpty() ? null : pParents.peek();
		for (FMNode child : pNode.getChildren())
		{
			final String label = child.getLabel();
			ChapNumType t = null;
			if (label.startsWith("#"))
				t = ChapNumType.NUMBER;
			else if (label.startsWith("$"))
				t = ChapNumType.ALPHAU;
			else if (label.startsWith("£"))
				t = ChapNumType.ALPHAL;
			else if (label.startsWith("!"))
				t = ChapNumType.ROMANU;
			else if (label.startsWith("?"))
				t = ChapNumType.ROMANL;

			final String[] chapNums = computeChapterNums(t == null ? null : CHAP_NUM_TYPES.get(t)[chapNum], parent);
			if (t != null)
				chapNum++;
			final EPubChapter chap = new EPubChapter(chapNums, computeChapId(idOrder++, pParents), child, pParents.isEmpty() ? idOrder - 1 : pParents.get(0).getChapFileNum());
			if (parent != null)
				parent.getChildren().add(chap);
			else
				pChapters.add(chap);
			pParents.push(chap);
			buildEPubChapters(child, pChapters, pParents);
			pParents.pop();
		}
	}

	private static int computeChapId(int pChapNum, Stack<EPubChapter> pParents)
	{
		int level = 1;
		for (int i = 0; i < pParents.size(); i++)
			level *= 100;
		final int parentId = pParents.isEmpty() ? 0 : pParents.peek().getChapId();
		return parentId + level * pChapNum;
	}

	private static String[] computeChapterNums(String pLabel, EPubChapter pParent)
	{
		final List<String> cn = pParent == null ? new ArrayList<>() : new ArrayList<>(Arrays.asList(pParent.getChapter()));
		cn.add(pLabel);
		return cn.toArray(new String[cn.size()]);
	}
}
