package jyt.freemind.converter.epub;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

// Source code to create a zip file from a given folder
// This example program recursively adds all files in the folder
// Works only with Java 7 and above
public class ZipFolder
{
	public static void main(String[] args) throws Exception
	{
//		ZipFolder zf = new ZipFolder();

		// Use the following paths for windows
		// String folderToZip = "c:\\demo\\test";
		// String zipName = "c:\\demo\\test.zip";

		// Linux/mac paths
		String folderToZip = "/Users/jj/test";
		String zipName = "/Users/jj/test.zip";
		ZipFolder.zipFolder(Paths.get(folderToZip), Paths.get(zipName));
	}

	// Uses java.util.zip to create zip file
	public static void zipFolder(Path sourceFolderPath, Path zipPath) throws Exception
	{
		ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipPath.toFile()));
		// Add the file "mimetype" FIRST as this is a requirement for epubs
		// Besides, that file should NOT be compressed - we need to calculate a bunch of stuff including length and CRC
		String mimetype = Files.readAllLines(sourceFolderPath.resolve("mimetype")).get(0);
		zos.setMethod(ZipOutputStream.STORED);
		zos.setLevel(0);
		ZipEntry ze = new ZipEntry("mimetype");
		ze.setSize(mimetype.length());
		ze.setCompressedSize(mimetype.length());
		CRC32 crc = new CRC32();
		crc.update(mimetype.getBytes(), 0, mimetype.length());
		ze.setCrc(crc.getValue());
		// Finally add it
		zos.putNextEntry(ze);
		Files.copy(sourceFolderPath.resolve("mimetype"), zos);
		zos.closeEntry();

		// Other entries are compressed to the max
		zos.setMethod(ZipOutputStream.DEFLATED);
		zos.setLevel(9);
		// Add all files except "mimetype" in their correct path
		Files.walkFileTree(sourceFolderPath, new SimpleFileVisitor<Path>()
		{
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
			{
				if (!"mimetype".equals(file.toFile().getName()))
				{
					zos.putNextEntry(new ZipEntry(sourceFolderPath.relativize(file).toString()));
					Files.copy(file, zos);
					zos.closeEntry();
				}
				return FileVisitResult.CONTINUE;
			}
		});
		zos.close();
	}
}