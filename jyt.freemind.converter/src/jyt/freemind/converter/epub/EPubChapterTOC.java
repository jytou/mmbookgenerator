package jyt.freemind.converter.epub;

/**
 * The TOC for a chapter: contains everything to add chapters to an Epub TOC.
 * @author jytou
 */
public class EPubChapterTOC
{
	// The unique ID for the chapter
	public String id;
	// The order in which this chapter must be placed
	public String playOrder;
	// The title of the chapter
	public String title;
	// The name of the file that contains the chapter's text
	public String contentFile;

	public EPubChapterTOC(String pId, String pPlayOrder, String pTitle, String pContentFile)
	{
		super();
		id = pId;
		playOrder = pPlayOrder;
		title = pTitle;
		contentFile = pContentFile;
	}

	public String getId()
	{
		return id;
	}

	public String getPlayOrder()
	{
		return playOrder;
	}

	public String getTitle()
	{
		return title;
	}

	public String getContentFile()
	{
		return contentFile;
	}

}
