package jyt.freemind.converter.epub;

import jyt.freemind.converter.NoteReference;

public class EpubNoteReference extends NoteReference
{
	private String mBackReference;
	private String mFullTxt;

	public EpubNoteReference(String pRefId, String pNoteId, String pBackReference, String pFullTxt)
	{
		super(pRefId, pNoteId, NoteRefType.GLOBAL);
		mBackReference = pBackReference;
		mFullTxt = pFullTxt;
	}

	public String getBackReference()
	{
		return mBackReference;
	}

	public String getFullTxt()
	{
		return mFullTxt;
	}
}
