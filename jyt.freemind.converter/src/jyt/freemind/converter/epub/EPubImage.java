package jyt.freemind.converter.epub;

public class EPubImage
{
	private static final String UNVANTED_CHARS = "./_";
	private String mSource;
	private String mId;
	private String mType;
	public EPubImage(String pSource, String pId, String pType)
	{
		super();
		mSource = pSource;
		mId = stripUnwantedChars(pId);
		mType = pType;
	}
	private String stripUnwantedChars(String pId)
	{
		StringBuilder sb = new StringBuilder("img");
		for (int i = 0; i < pId.length(); i++)
			if (UNVANTED_CHARS.indexOf(pId.charAt(i)) == -1)
				sb.append(pId.charAt(i));
		return sb.toString();
	}
	public String getSource()
	{
		return mSource;
	}
	public String getId()
	{
		return mId;
	}
	public String getType()
	{
		return mType;
	}
}
