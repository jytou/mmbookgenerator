package jyt.freemind.converter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.StringTokenizer;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import jyt.freemind.converter.FMParagraph.Property;

/**
 * Reads a FreeMind .mm file and fills:
 * - FMNodes with their contents,
 * - metadata from the mm file,
 * - various formats from the mm file
 * @author jytou
 */
public class SAXHandler extends DefaultHandler
{
	final static private String sStartNodeName = "$Main$";
	final static private String sMetaNodeName = "$Meta$";
	final static private String sFormatsNodeName = "$Formats$";

	private FMDocument mDocument;
	// Used when parsing the document
	private FMNode mCurrentNode = null;
	// The current paragraph's properties
	private Map<Property, String> mParagraphProperties = new HashMap<>();
	// Whether to capture text or not
	private boolean mCapture = false;
	// When grabbing text, the current text
	private StringBuilder mCurrentText = null;
	// Whether to capture metadata (eg. we are under the $Meta$ node)
	private boolean mCollectMeta = false;
	// Current metadata being captured as text
	private String mCurrentMeta = null;
	// Whether to capture formats (eg. we are under the $Formats$ node)
	private boolean mCollectFormat = false;
	// Current format being captured
	private String mCurrentFormatName = null;
	// Current format properties being captured
	private Map<String, String> mCurrentFormatProperties = null;
	// The stack of current labels
	private Stack<String> mLabels = new Stack<>();
	// [note id, [path where it was found first]]
	private Map<String, Stack<String>> mNoteIdPaths = new HashMap<>();

	public SAXHandler(FMDocument pDocument)
	{
		super();
		mDocument = pDocument;
	}

	@Override
	public void startElement(String pUri, String pLocalName, String pQName, Attributes pAttributes) throws SAXException
	{
		super.startElement(pUri, pLocalName, pQName, pAttributes);
		if ("node".equals(pQName))
		{
			// Capturing a node, grab its label
			final String label = pAttributes.getValue("TEXT");
			mLabels.push(label);
			if (sStartNodeName.equals(label))
			// We found the root node
				mDocument.setRootNode(mCurrentNode = new FMNode(null, sStartNodeName));
			else if (sMetaNodeName.equals(label))
			// We found the $Meta$ node
				mCollectMeta = true;
			else if (sFormatsNodeName.equals(label))
			// We found the $Formats$ node
				mCollectFormat = true;
			else if (mCollectMeta)
			// We need to collect metadata
				mCurrentMeta = label;
			else if (mCollectFormat)
			// We need to collect formats
			{
				mCurrentFormatName = label;
				mCurrentFormatProperties = new HashMap<>();
			}
			else if (mCurrentNode != null)
				mCurrentNode = new FMNode(mCurrentNode, label);
		}
		else if ("p".equals(pQName) || "ul".equals(pQName))
		// That's a paragraph in a node
		{
			if (mCollectFormat || mCollectMeta || (mCurrentNode != null))
			// This is some metadata
			{
				mCapture = true;
				if (mCollectMeta && (mCurrentText != null))
				// Metadata doesn't support paragraphs, it is only using one String
					mCurrentText.append("<br>");
			}
			if ((!mCollectMeta) && (!mCollectFormat) && (mCurrentNode != null))
			// This is normal text
			{
				final String props = pAttributes.getValue("style");
				// Process styles to retain the different styles - currently only alignment
				if (props != null)
				{
					final StringTokenizer stp = new StringTokenizer(props, ",");
					while (stp.hasMoreTokens())
					{
						final String tokp = stp.nextToken().trim();
						final int indexOfSemicolon = tokp.indexOf(':');
						if (indexOfSemicolon > 0)
						{
							final String name = tokp.substring(0, indexOfSemicolon).trim();
							final String value = tokp.substring(indexOfSemicolon + 1).trim();
							// Add the properties in the paragraph's properties
							if ("text-align".equals(name))
								mParagraphProperties.put(FMParagraph.Property.JUSTIFICATION, value);
							else
								System.err.println("WARNING: property " + tokp + " not recognized");
						}
						else
							System.err.println("WARNING: property " + tokp + " not recognized");
					}
				}
			}
		}
		else if (mCapture)
		// If we're currently capturing some text, then capture html elements as well as we want them to get through
		{
			mCurrentText.append('<').append(pQName);
			final int nbAttr = pAttributes.getLength();
			for (int i = 0; i < nbAttr; i++)
				mCurrentText.append(' ').append(pAttributes.getQName(i)).append("=\"").append(pAttributes.getValue(i)).append("\"");
			mCurrentText.append('>');
		}
	}

	@Override
	public void endElement(String pUri, String pLocalName, String pQName) throws SAXException
	{
		super.endElement(pUri, pLocalName, pQName);
		if ("node".equals(pQName))
		{
			// Ending a node
			final String label = mLabels.pop();
			final String labelLower = label.toLowerCase();
			if (labelLower.startsWith("$note$"))
			{
				// it is not a normal node, it is a note, transform the current node into a note of the document
				final String noteId = label.substring(label.indexOf('$', 1) + 1).trim();
				if (noteId.indexOf(' ') != -1)
					throw new SAXException("Note ID \"" + noteId + "\" contains a space, which is forbidden in ids");
				final List<FMParagraph> paragraphs = mCurrentNode.getParagraphs();
				if ((paragraphs == null) || paragraphs.isEmpty())
					throw new SAXException("Found empty note at " + mLabels.toString() + " / " + mCurrentNode.getLabel());
				final FMNote currentNote = new FMNote(noteId, paragraphs);
				int txtSize = 0;
				for (FMParagraph paragraph : paragraphs)
					txtSize += paragraph.getText().trim().length();
				if (txtSize == 0)
					throw new SAXException("Found empty note at " + mLabels.toString() + " / " + mCurrentNode.getLabel());
				final Map<String, FMNote> notes = mDocument.getNotes();
				if (notes.containsKey(noteId))
					throw new SAXException("Note ID already exists: " + noteId + "\nFound first at: " + mNoteIdPaths.get(noteId).toString() + "\nFound now at: " + mLabels.toString());
				// save the current note's path
				final Stack<String> stackMemory = new Stack<>();
				stackMemory.addAll(mLabels);
				mNoteIdPaths.put(currentNote.getId(), stackMemory);
				// Add the note into the document
				mDocument.getNotes().put(noteId, currentNote);
				// remove that node from the tree
				mCurrentNode.getParent().getChildren().remove(mCurrentNode);
			}
			if (mCurrentNode != null)
				mCurrentNode = mCurrentNode.getParent();
			else if (mCollectMeta && sMetaNodeName.equals(label))
			{
				mCollectMeta = false;
			}
			else if (mCollectFormat && sFormatsNodeName.equals(label))
			{
				mCollectFormat = false;
			}
			else if (mCollectMeta)
			{
				// We finished collecting some metadata - add it
				mDocument.getMetas().put(mCurrentMeta, mCurrentText == null ? "" : mCurrentText.toString().trim());
				mCurrentMeta = null;
				mCurrentText = null;
			}
			else if (mCollectFormat)
			{
				// We finished collecting a format - add it
				mDocument.getFormats().put(mCurrentFormatName, mCurrentFormatProperties);
				mCurrentFormatName = null;
				mCurrentFormatProperties = null;
			}
		}
		else if ("p".equals(pQName) || "ul".equals(pQName))
		{
			// Finished collecting some text
			mCapture = false;
			if (mCollectFormat)
			{
				// This is a format, we have to add a new property in the current format
				final String txt = mCurrentText.toString().trim();
				if (txt.indexOf('=') == -1)
					throw new SAXException("Property " + txt + " must have an equal sign in format " + mCurrentFormatName);
				final String propName = txt.substring(0, txt.indexOf('='));
				final String propValue = txt.substring(txt.indexOf('=') + 1);
				mCurrentText = null;
				mCurrentFormatProperties.put(propName, propValue);
			}
			else if ((mCurrentNode != null) && (mCurrentText != null) && (mCurrentText.length() > 0))
			{
				// This is normal text, first process various special tags
				String txt = mCurrentText.toString().trim();
				if (txt.indexOf("[vcenter]") != -1)
				{
					txt = txt.replace("[vcenter]", "");
					mParagraphProperties.put(Property.VJUST, "");
				}
				if (txt.indexOf("[citation]") != -1)
				{
					txt = txt.replace("[citation]", "");
					mParagraphProperties.put(Property.CITATION, "");
				}
				if (txt.indexOf("[caption]") != -1)
				{
					txt = txt.replace("[caption]", "");
					mParagraphProperties.put(Property.CAPTION, "");
				}
				if (txt.indexOf("[cadre]") != -1)
				{
					txt = txt.replace("[cadre]", "");
					mParagraphProperties.put(Property.CADRE, "");
				}
				int pos = 0;
				while ((pos = txt.indexOf('[', pos)) != -1)
				{
					if (txt.indexOf(']', pos) != -1)
					{
						final String betweenBrackets = txt.substring(pos + 1, txt.indexOf(']', pos));
						if ((!"...".equals(betweenBrackets)) && (!"…".equals(betweenBrackets)) && (!"&#8230;".equals(betweenBrackets)))
							txt = txt.substring(0, txt.indexOf('[')) + txt.substring(txt.indexOf(']') + 1);
						else
							pos++;
					}
					else
						pos++;
				}

				if ("ul".equals(pQName))
				{
					// This is a list
					int liPos = 0;
					while ((liPos = txt.indexOf("<li>", liPos)) >= 0)
					{
						liPos += "<li>".length();
						final int liEnd = txt.indexOf("</li>", liPos);
						if (liEnd > liPos)
							addParagraph(txt.substring(liPos, liEnd)).setProperty(Property.LIST, true);
					}
				}
				else
					addParagraph(txt);
				mCurrentText = null;
			}
		}
		else if (mCapture) // we're still capturing text, add the tag end - pass through
			mCurrentText.append("</").append(pQName).append('>');
	}

	private FMParagraph addParagraph(String pTxt)
	{
		// Create the paragraph and set its properties
		final FMParagraph paragraph = new FMParagraph(pTxt, 0, 0);
		if (!mParagraphProperties.isEmpty())
		{
			for (Property key : mParagraphProperties.keySet())
				paragraph.setProperty(key, mParagraphProperties.get(key));
			mParagraphProperties.clear();
		}
		mCurrentNode.addParagraph(paragraph);
		return paragraph;
	}

	@Override
	public void characters(char[] pCh, int pStart, int pLength) throws SAXException
	{
		super.characters(pCh, pStart, pLength);
		if (mCapture)
		{
			if (mCurrentText == null)
				mCurrentText = new StringBuilder();
			mCurrentText.append(pCh, pStart, pLength);
		}
	}
}
