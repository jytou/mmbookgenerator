package jyt.freemind.converter;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A paragraph from a FreeMind file.
 * @author jytou
 */
public class FMParagraph
{
	// Different properties that a paragraph can have
	public enum Property
	{
		JUSTIFICATION("text-align"),// horizontal justification of the whole text for this paragraph
		VJUST(""),// vertical justification for this paragraph
		FIRST_INDENT("  "),// indent for the first line of the paragraph - default is 2 spaces
		PAR_INDENT(""),// indentation for the whole paragraph
		CITATION(""),// whether this paragraph is part of a quote or not
		CADRE(""),// whether this paragraph should be enclosed in a frame
		CAPTION(""),// whether this paragraph is a caption (eg. no indent and possibly numbering)
		LIST(""),
		TITLE1(""),// A title of level 1, this is used to glue titles with lower-level titles but not same level
		TITLE2(""),// A title of level 2, this is used to glue titles with lower-level titles but not same level
		TITLE3(""),// A title of level 3, this is used to glue titles with lower-level titles but not same level
		TITLE4(""),// A title of level 4, this is used to glue titles with lower-level titles but not same level
		TITLE5("");// A title of level 5, this is used to glue titles with lower-level titles but not same level
		String mRepresentation;
		Property(String pRepresentation)
		{
			mRepresentation = pRepresentation;
		}
		public String getRepresentation()
		{
			return mRepresentation;
		}
	};

	// Full text
	private String mText;
	// Properties
	private Map<Property, Object> mProperties = Collections.emptyMap();
	// Amount of space before the paragraph
	private float mSpaceBefore;
	// Amount of space after the paragraph
	private float mSpaceAfter;

	public FMParagraph(String pText, float pSpaceBefore, float pSpaceAfter)
	{
		super();
		mText = pText;
		mSpaceBefore = pSpaceBefore;
		mSpaceAfter = pSpaceAfter;
	}

	public boolean hasProperty(Property pName)
	{
		return mProperties.containsKey(pName);
	}

	public void setProperty(Property pName, Object pValue)
	{
		if (mProperties.isEmpty())
		// because it is originally an empty map that is immutable
			mProperties = new HashMap<>();
		mProperties.put(pName, pValue);
	}

	public Set<Property> getProperties()
	{
		return mProperties.keySet();
	}

	public Object getPropertyValue(Property pName)
	{
		return mProperties.get(pName);
	}

	public String getText()
	{
		return mText;
	}

	public void setText(String pText)
	{
		mText = pText;
	}

	public float getSpaceAfter()
	{
		return mSpaceAfter;
	}

	public float getSpaceBefore()
	{
		return mSpaceBefore;
	}
}
