<map version="freeplane 1.9.13">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="titre" FOLDED="false" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1537308615218" background="#000000"><hook NAME="MapStyle" background="#000000" zoom="0.826">
    <properties show_icon_for_attributes="true" edgeColorConfiguration="#808080ff,#808080ff,#808080ff,#808080ff,#808080ff" show_notes_in_map="false" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_1988239149" MAX_WIDTH="600 px" COLOR="#33ff33" BACKGROUND_COLOR="#000000" STYLE="as_parent">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1988239149" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="SansSerif" SIZE="11" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#4e85f8" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#4e85f8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#ffff00">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#ff9999">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#66ffff" BACKGROUND_COLOR="#003333">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="35" RULE="ON_BRANCH_CREATION"/>
<node TEXT="$Main$" POSITION="right" ID="ID_127052122" CREATED="1530097371356" MODIFIED="1536702397373">
<edge COLOR="#808080"/>
<node TEXT="" ID="ID_873305541" CREATED="1533315314157" MODIFIED="1694151212966"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      [vcenter]<font size="6"><i>Mon </i><b>super </b>titre</font>
    </p>
    <p style="text-align: center">
      [vcenter]
    </p>
    <p style="text-align: center">
      [vcenter]Mon Éditeur
    </p>
    <p style="text-align: center">
      [vcenter]Septembre 2018
    </p>
    <p style="text-align: center">
      [vcenter]ISBN : $ISBN$
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="" ID="ID_277865512" CREATED="1532271109429" MODIFIED="1689944466550"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      [vcenter]Remerciements
    </p>
    <p>
      [vcenter]
    </p>
    <p>
      [vcenter]Je tiens à remercier ma famille, mon chat, mes voisins, pour «&nbsp;<i>leur</i>&nbsp;» grande patience.
    </p>
    <p>
      [vcenter]Et aussi tous ceux que je ne veux pas citer, de peur <i>d'oublier</i>, malencontreusement, le nom d'un grincheux qui va me maudire pour l'éternité.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Test de chapitres" ID="ID_1842752027" CREATED="1703784432177" MODIFIED="1704325908240">
<arrowlink DESTINATION="ID_1842752027"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      toto
    </p>
  </body>
</html></richcontent>
<node TEXT="Prems" ID="ID_890883885" CREATED="1703784474924" MODIFIED="1704492737167"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      tata
    </p>
  </body>
</html></richcontent>
<node TEXT="Uno" ID="ID_869784003" CREATED="1704323599535" MODIFIED="1704323636206"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Beaucoup de blabla. Et plus de blabla.
    </p>
    <p>
      Beaucoup de blabla. Et plus de blabla.
    </p>
    <p>
      Beaucoup de blabla. Et plus de blabla.
    </p>
    <p>
      Beaucoup de blabla. Et plus de blabla.
    </p>
    <p>
      Beaucoup de blabla. Et plus de blabla.
    </p>
    <p>
      Beaucoup de blabla. Et plus de blabla.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Dos" ID="ID_1231608008" CREATED="1704323602308" MODIFIED="1705280325005"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      1 Beaucoup de blabla. Et plus de blabla.
    </p>
    <p>
      2 Beaucoup de blabla. Et plus de blabla.
    </p>
    <p>
      3 Beaucoup de blabla. Et plus de blabla.
    </p>
    <p>
      4 Beaucoup de blabla. Et plus de blabla.
    </p>
    <p>
      5 Beaucoup de blabla. Et plus de blabla.{fn:algododo}
    </p>
    <p>
      6 Beaucoup de blabla. Et plus de blabla.{fn:algodada}
    </p>
    <p>
      7 Beaucoup de blabla. Et plus de blabla.{fn:algodidi}
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Tres" ID="ID_456020996" CREATED="1704323606706" MODIFIED="1704323643065"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Beaucoup de blabla. Et plus de blabla.
    </p>
    <p>
      Beaucoup de blabla. Et plus de blabla.
    </p>
    <p>
      Beaucoup de blabla. Et plus de blabla.
    </p>
    <p>
      Beaucoup de blabla. Et plus de blabla.
    </p>
    <p>
      Beaucoup de blabla. Et plus de blabla.
    </p>
    <p>
      Beaucoup de blabla. Et plus de blabla.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="$note$ algododo" ID="ID_853674487" CREATED="1704325963377" MODIFIED="1704492719366"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      algo más
    </p>
    <p>
      y más
    </p>
    <p>
      y otros
    </p>
    <p>
      qué más
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="$note$ algodada" ID="ID_1157307746" CREATED="1704325963377" MODIFIED="1705280233233"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      otras informaciones
    </p>
    <p>
      que contienen multiples parágrafos
    </p>
    <p>
      y uno más
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="$note$ algodidi" ID="ID_1565033719" CREATED="1704325963377" MODIFIED="1705280302822"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      el didi
    </p>
    <p>
      está muy interesante
    </p>
    <p>
      y también toma muchos parágrafos en multiples líneas que contienen mucho blablabla y más blablabla
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Second" ID="ID_977160596" CREATED="1703784478322" MODIFIED="1703784543998"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Et une deuxième page de blabla.
    </p>
    <p>
      Et une deuxième page de blabla.
    </p>
    <p>
      Et une deuxième page de blabla.
    </p>
    <p>
      Et une deuxième page de blabla.
    </p>
    <p>
      Et une deuxième page de blabla.
    </p>
    <p>
      Et une deuxième page de blabla.
    </p>
    <p>
      Et une deuxième page de blabla.
    </p>
    <p>
      Et une deuxième page de blabla.
    </p>
    <p>
      Et une deuxième page de blabla.
    </p>
    <p>
      Et une deuxième page de blabla.
    </p>
    <p>
      Et une deuxième page de blabla.
    </p>
    <p>
      Et une deuxième page de blabla.
    </p>
    <p>
      Et une deuxième page de blabla.
    </p>
    <p>
      Et une deuxième page de blabla.
    </p>
    <p>
      Et une deuxième page de blabla.
    </p>
    <p>
      Et une deuxième page de blabla.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Tiers" ID="ID_444185031" CREATED="1703784545015" MODIFIED="1703784576600"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Une troisième page… juste pour faire une troisième page.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Tests basiques" ID="ID_805442818" CREATED="1537405230898" MODIFIED="1703797679737"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      J'aimerais vraiment savoir si le 10<sup>ème </sup>&nbsp;élément de la chaîne se termine correctement. D'autre part, des lignes suivantes du même paragraphe{fn:toto} devraient également fonctionner correctement, avec du <b>gras</b>, de <i>l'italique</i>, ou des caractères normaux, tout simplement.
    </p>
    <p>
      J'aimerais <b>vraiment </b>savoir <i>si </i>le 10<sup>ème </sup>élément <b>de </b>la <i>chaîne </i><b>se </b><i>termine </i>correctement.
    </p>
    <p>
      Des dialogues{fn:notedial}&nbsp;:
    </p>
    <p>
      −&nbsp;Question.
    </p>
    <p>
      −Réponse.
    </p>
    <p>
      —Réaction.
    </p>
    <p>
      — Finalisation.
    </p>
    <p>
      «&nbsp;Sais-tu&nbsp;?&nbsp;» «&nbsp;Je ne sais pas&nbsp;».
    </p>
    <p>
      Voyons si des caractères comme Ça, ou Ğ ou γ fonctionnent. Et du russe, можно{fn:mozno}&nbsp;?
    </p>
  </body>
</html></richcontent>
<node TEXT="$note$ toto" ID="ID_1359564160" CREATED="1689870610862" MODIFIED="1692518117454"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      blah blah blah blah blah
    </p>
    <p>
      et une deuxième ligne pour Toto&nbsp;!
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="$note$ notedial" ID="ID_877581092" CREATED="1689870908307" MODIFIED="1692518139578"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      une note avant un espace insécable qui est décidément très longue et qui devrait arriver à faire facilement deux lignes, voire plus
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="$note$ mozno" ID="ID_1151989134" CREATED="1689879177847" MODIFIED="1689935281439"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      C'est possible = それが可能だ
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="#Lorem Ipsum" ID="ID_72262714" CREATED="1532035391029" MODIFIED="1537308815146">
<node TEXT="What is Lorem Ipsum?" ID="ID_1748022793" CREATED="1532034988552" MODIFIED="1689935967457"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Lorem Ipsum</b>{fn:lorem-ipsum} is simply <b><i>the</i></b>&nbsp;dummy text of the printing and typesetting industry. <i>Lorem Ipsum</i>&nbsp;has been the industry's standard dummy text ever since the 1500s{fn:1500s}{fn:oh-by-the-way}, when an unknown printer{gn:earlier} took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing <i>Lorem Ipsum </i>passages, and more recently with desktop publishing software like <b>Aldus PageMaker </b>including versions of <i>Lorem Ipsum</i>.
    </p>
  </body>
</html></richcontent>
<node TEXT="$note$ lorem-ipsum" ID="ID_241508742" CREATED="1689670685228" MODIFIED="1689845689905"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      See all the following descriptions
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="$note$ 1500s" ID="ID_1717618975" CREATED="1689670759358" MODIFIED="1689845694792"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Or even maybe earlier
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="$note$ earlier" ID="ID_525387637" CREATED="1689845735485" MODIFIED="1689845744240"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      some end note
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="$note$ oh-by-the-way" ID="ID_1484906436" CREATED="1689868854401" MODIFIED="1689868870717"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      I forgot to mention that other one!
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Where does it come from?" ID="ID_1422412736" CREATED="1532035121480" MODIFIED="1537703967599"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Contrary to popular belief, <i>Lorem Ipsum </i>is not simply random text. It has roots in a piece of classical Latin literature from <b>45 BC</b>, making it over 2000 years old. <b>Richard McClintock</b>, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, <i>consectetur</i>, from a <i>Lorem Ipsum </i>passage, and going through the cites of the word in classical literature, discovered the undoubtable source. <i>Lorem Ipsum </i>comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by <b>Cicero</b>, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of <i>Lorem Ipsum</i>, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.
    </p>
    <p>
      The standard chunk of <i>Lorem Ipsum </i>used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Why do we use it?" ID="ID_425373909" CREATED="1532035267582" MODIFIED="1537703982166"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using <i>Lorem Ipsum </i>is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use <i>Lorem Ipsum </i>as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Where can I get some?" ID="ID_113479111" CREATED="1532035302270" MODIFIED="1537704036360"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      There are many variations of passages of <i>Lorem Ipsum </i>available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of <i>Lorem Ipsum</i>, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the <i>Lorem Ipsum </i>generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate <i>Lorem Ipsum </i>which looks reasonable. The generated <i>Lorem Ipsum </i>is therefore always free from repetition, injected humour, or non-characteristic words etc.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="#Examples" ID="ID_1891160724" CREATED="1530093635600" MODIFIED="1537308960400">
<node TEXT="##The standard Lorem Ipsum passage, used since the 1500s" ID="ID_1952472668" CREATED="1532035427510" MODIFIED="1537309029961"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="##Section 1.10.32 of &quot;de Finibus Bonorum et Malorum&quot;, written by Cicero in 45 BC" ID="ID_1993264380" CREATED="1532035476948" MODIFIED="1537309038562"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?&quot;
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="##1914 translation by H. Rackham" ID="ID_1759558228" CREATED="1532035522973" MODIFIED="1537309046343"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="##Section 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot;, written by Cicero in 45 BC" ID="ID_1331042523" CREATED="1532035560653" MODIFIED="1537309051654"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.&quot;
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="##1914 translation by H. Rackham" ID="ID_1388374983" CREATED="1532035593516" MODIFIED="1537309057813"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.&quot;
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="#Full Text" ID="ID_1514179728" CREATED="1532037524110" MODIFIED="1537309089773">
<node TEXT="##Lorem Ipsum" ID="ID_250361136" CREATED="1532037544302" MODIFIED="1539258065576"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      <i>[citation]&quot;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...&quot; </i>
    </p>
    <p style="text-align: center">
      <i>[citation]&quot;There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...&quot;</i>
    </p>
    <p>
      
    </p>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla semper leo in dictum porta. Integer sed leo ultrices, tristique magna sed, tincidunt quam. Pellentesque odio elit, pharetra et justo sed, condimentum luctus augue. Donec placerat eleifend purus, nec rutrum est sagittis quis. Curabitur cursus vulputate blandit. Nam ultrices neque nisl, non maximus risus dignissim nec. Aliquam dignissim libero vel tortor hendrerit ullamcorper. Ut porta porta varius. Mauris auctor dui eu viverra consectetur. Aliquam sed tempus odio. Etiam tempor turpis quis orci condimentum, eget dignissim neque auctor. Vivamus tempus mauris sit amet neque tempor, sed auctor nisl varius.
    </p>
    <p>
      Aenean eu scelerisque nulla, sed ultrices orci. Vestibulum dignissim leo non faucibus efficitur. Curabitur laoreet, magna quis viverra feugiat, dolor arcu sollicitudin velit, eu venenatis nulla nisi vitae metus. Donec ultricies nisi eu tempus accumsan. Maecenas vulputate, sapien vitae tempor convallis, magna est interdum tellus, vitae ornare lacus magna ut quam. Vivamus ac rutrum nunc. Integer eleifend enim sapien, in varius orci condimentum et. Fusce rhoncus magna vitae gravida rutrum. Integer ultrices vel urna ac imperdiet. Integer lacinia, neque vel aliquam venenatis, enim nulla mollis dolor, vel pellentesque nulla justo vitae turpis. Praesent suscipit ipsum sed magna scelerisque cursus. Proin eu augue pellentesque, commodo lorem in, ultricies augue. Duis non velit nunc. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    </p>
    <p>
      Proin elementum vel mi sed mollis. Nam feugiat ipsum ac auctor volutpat. Sed quis volutpat arcu. Donec pretium, purus vitae consequat facilisis, dui nulla consequat purus, et sagittis neque felis ac dui. Fusce viverra turpis eu nulla aliquam porta. Nulla facilisi. In id justo quis lorem dignissim finibus. Phasellus tempus tincidunt mi ut ultricies. Vestibulum iaculis accumsan ex, a rutrum nunc aliquet vel. Fusce justo elit, placerat ac est rhoncus, consectetur scelerisque ante. Nulla finibus ac lacus id blandit. Donec ante justo, mollis id bibendum at, ultricies vitae ex.
    </p>
    <p>
      Donec rutrum elit in massa facilisis, at feugiat velit dapibus. Donec tristique suscipit commodo. Sed pretium, augue eget tincidunt elementum, massa tellus cursus est, nec viverra lacus ipsum et lorem. Aliquam dignissim laoreet turpis, posuere cursus ex hendrerit eu. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin id massa sit amet mi venenatis condimentum. Suspendisse potenti. Vestibulum finibus iaculis magna, eu efficitur neque consequat eget. Proin vestibulum efficitur enim, a maximus ipsum. Aliquam vehicula rutrum rutrum. Integer gravida neque purus, sed sagittis felis finibus vitae. Fusce eu enim purus. Aenean euismod sodales quam sed tincidunt. Phasellus luctus, felis at auctor blandit, nisl ligula venenatis est, vel hendrerit orci augue ac arcu. Nam ex felis, mattis sed euismod eu, molestie vitae massa. Aenean a egestas elit.
    </p>
    <p>
      Morbi hendrerit feugiat nunc, et vestibulum ligula interdum eget. Aenean ultrices velit neque, pretium ultrices nunc maximus in. Integer vitae leo eget massa placerat egestas. Ut lacinia vel eros ac rhoncus. Maecenas eleifend leo metus, imperdiet pellentesque nibh ullamcorper at. Cras hendrerit ante et ultrices posuere. Nunc viverra, ipsum at dignissim interdum, leo lectus accumsan nunc, quis maximus elit nibh a ligula. Donec at tortor vel tellus hendrerit dapibus in id ligula. Aliquam erat volutpat. Donec vestibulum felis aliquam mi eleifend auctor. Nunc porttitor cursus lacus vel facilisis. Nulla facilisi. Duis nec elit a nisl scelerisque finibus ut in nisi. Ut pharetra ex ligula, nec tincidunt enim rhoncus vel. Integer rutrum dolor id leo finibus placerat. Cras aliquam porta metus at dapibus.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="##Lorem Ipsum" ID="ID_416852488" CREATED="1532037638438" MODIFIED="1539258077484"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      <i>[citation]&quot;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...&quot; </i>
    </p>
    <p style="text-align: center">
      <i>[citation]&quot;There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...&quot;</i>
    </p>
    <p>
      
    </p>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pellentesque diam non rutrum bibendum. Proin pretium lacus erat, at semper nibh viverra at. Donec a justo eros. Fusce vitae nisl lobortis, accumsan nulla ut, rhoncus urna. Vestibulum a lectus eget velit suscipit mattis facilisis tempor nisi. Nam tincidunt orci ac placerat posuere. Morbi in tincidunt justo. Aliquam erat volutpat. Phasellus vitae sapien non justo finibus auctor. Fusce sit amet mauris lacus. Vestibulum eleifend porta eros, sed sodales nisl eleifend in. Pellentesque quis mi nec orci ullamcorper aliquet eget quis arcu. Donec tincidunt tellus ac massa gravida euismod. Cras egestas malesuada turpis, id vehicula orci. Suspendisse vel bibendum ipsum, in luctus augue.
    </p>
    <p>
      In semper nec dolor a laoreet. Aenean dignissim fringilla congue. Phasellus lacinia luctus neque non sollicitudin. Nam et suscipit odio. Proin ultrices dignissim eros at pellentesque. Sed nec pharetra risus. Maecenas vel venenatis nibh, vitae euismod nisl. Mauris leo ex, mollis sit amet orci vel, ornare consequat magna. In in convallis libero.
    </p>
    <p>
      Nunc faucibus, mauris porta consectetur consequat, ante eros posuere elit, ac aliquet magna elit vel justo. Fusce quis diam cursus, dictum metus eu, ornare dui. Sed pulvinar magna ornare ipsum venenatis mattis. Aenean eget libero leo. Aenean non felis eget massa condimentum rutrum. Aenean lorem nulla, interdum in lectus et, porttitor finibus justo. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum luctus maximus turpis, sed condimentum sapien blandit et. Aenean eget lectus enim. Pellentesque lacinia ante id nunc consequat volutpat. Nunc sit amet sem id nisi sodales ornare. Donec tempor nisi ante, eu laoreet nulla suscipit et. Duis non suscipit ex. Quisque hendrerit pulvinar nisi, vel sollicitudin mi sagittis ac. Aliquam erat volutpat. Sed purus ligula, sodales eu pellentesque non, congue non quam.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Postface" ID="ID_940167567" CREATED="1533761692908" MODIFIED="1537309313674"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris egestas dolor bibendum, interdum nisl nec, gravida ipsum. Nulla tincidunt aliquet odio vitae tempor. Ut id urna eget nibh aliquet consequat. Nam pulvinar erat vitae viverra aliquam. Nulla faucibus in nisl id consequat. Maecenas posuere elementum lectus ac aliquet. Nullam gravida commodo elit, a tristique libero elementum a. Aenean id dolor arcu. Nullam consequat efficitur ligula, at malesuada urna efficitur eu. Aenean eget nulla nulla. Donec diam leo, feugiat non lectus non, tempus ullamcorper ex. Integer a mauris vitae sem porttitor consectetur.
    </p>
    <p>
      Morbi ullamcorper nibh feugiat viverra pharetra. Pellentesque non enim non odio vehicula vehicula in id eros. Sed id pharetra nunc. Aenean ullamcorper finibus vestibulum. Nam sit amet orci lectus. Donec ex leo, pharetra et neque ut, aliquam congue lectus. Etiam auctor varius tristique. Maecenas faucibus finibus turpis, sed cursus ante tincidunt sed. Vestibulum at augue vulputate, congue mi ac, dignissim nulla. Nulla malesuada mollis mi in porttitor. Duis rutrum urna gravida purus mollis eleifend. Quisque cursus finibus ligula nec faucibus. Vivamus cursus placerat quam vel finibus.
    </p>
    <p>
      Nunc feugiat ornare nulla vel fringilla. Maecenas eu ex vel ligula commodo feugiat at in neque. Sed ultricies at nunc vitae sodales. Praesent sed diam malesuada, elementum dui ut, venenatis est. Sed est augue, blandit sed libero sed, mattis condimentum nisl. Curabitur sed mollis erat, id commodo nisi. Donec eget ex gravida, venenatis massa quis, mollis felis.
    </p>
    <p>
      Aenean eleifend metus et tempor scelerisque. Vivamus aliquam finibus velit, rhoncus dignissim leo iaculis nec. Praesent et laoreet nisl, id eleifend dolor. Morbi ac lobortis lectus, quis maximus sapien. Aliquam cursus metus sed massa iaculis, eget sagittis nunc varius. Fusce sapien neque, pellentesque sit amet turpis sed, facilisis convallis est. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse elit metus, bibendum ut lacinia vitae, efficitur non justo. Nunc laoreet congue libero, nec venenatis sapien malesuada quis. Vivamus volutpat ultricies magna, in volutpat ex finibus at. Nullam ut viverra elit. Nullam tempor cursus augue et cursus. Sed posuere, nunc a porttitor condimentum, massa lacus rutrum nisl, volutpat scelerisque massa urna ac ligula. Ut consequat nulla ac finibus aliquet. Proin semper id quam in semper. Curabitur sed leo urna.
    </p>
    <p>
      Vestibulum placerat quam eu elit dapibus hendrerit. In hendrerit felis vel venenatis luctus. Morbi tincidunt urna tortor, ut dapibus magna faucibus et. Praesent dolor arcu, sagittis et iaculis non, venenatis vel nibh. Sed faucibus tincidunt nisi, eget convallis eros molestie sed. Quisque non vestibulum ex. Vivamus eget leo accumsan, efficitur metus eu, dignissim urna. Vivamus nec turpis lectus. Nulla mollis eros vitae urna hendrerit sollicitudin. Integer vel ex turpis.
    </p>
    <p>
      Aenean ut dui sed diam dictum interdum vitae ut erat. Ut pretium id dolor ut varius. Sed quis purus ullamcorper, pellentesque ex non, lobortis tellus. Ut auctor tempor nisl, at malesuada ligula dictum nec. Pellentesque porta lorem eu nunc lacinia, imperdiet sollicitudin mi blandit. Vivamus id dapibus nisl, efficitur volutpat dui. Ut et enim quis justo tincidunt congue nec vel metus. Proin placerat elementum neque sit amet commodo. Quisque facilisis ex tortor, vel vehicula nisi venenatis vel. Fusce fermentum cursus orci vitae dignissim. Praesent eros risus, volutpat sit amet dolor eget, ultricies sollicitudin ante. Phasellus varius leo tortor, vestibulum fringilla ante aliquet blandit. Nam non bibendum mauris, non facilisis orci. Nulla facilisi.
    </p>
    <p>
      Vivamus venenatis mi et finibus dictum. Donec congue eget felis id aliquam. Nullam quis ipsum in velit interdum rhoncus. Etiam imperdiet nisi neque, sit amet elementum magna egestas eu. Aliquam dignissim at risus sollicitudin posuere. Sed pulvinar, diam nec ultricies ultrices, lorem velit laoreet elit, sit amet ultrices nibh lorem quis massa. Phasellus a odio sollicitudin, varius justo eu, finibus libero. Praesent ut urna ac sapien sodales vestibulum sit amet pharetra nisi. Sed eu ullamcorper neque. Nulla bibendum ut nulla at eleifend. Nunc in lacus augue. Integer auctor nisi ac consequat rutrum.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="un nœud de test" POSITION="left" ID="ID_1331873377" CREATED="1536851403607" MODIFIED="1537308546574">
<edge COLOR="#808080"/>
<node TEXT="pourquoi" ID="ID_970874397" CREATED="1536851411973" MODIFIED="1537308556738"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      pour voir
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="autre" ID="ID_1872733666" CREATED="1536854367483" MODIFIED="1537308559095"/>
</node>
<node TEXT="$Formats$" POSITION="left" ID="ID_1987404548" CREATED="1539255516600" MODIFIED="1539255523315">
<edge COLOR="#808080"/>
<node TEXT="epub" ID="ID_1624516035" CREATED="1539255524382" MODIFIED="1539255531457"/>
<node TEXT="pdf" ID="ID_824989331" CREATED="1539255531993" MODIFIED="1704323754461"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      ISBN=123-4-5678901-2-3
    </p>
    <p>
      PageWidth=148.5
    </p>
    <p>
      PageHeight=210
    </p>
    <p>
      TopMargin=15
    </p>
    <p>
      BottomMargin=8
    </p>
    <p>
      InnerMargin=15
    </p>
    <p>
      OuterMargin=10
    </p>
    <p>
      NormalFontSize=12
    </p>
    <p>
      Interlines=2
    </p>
    <p>
      FontNameNotes=NishikiTeki
    </p>
    <p>
      ExtraInterPar=0.7
    </p>
    <p>
      TitleFontSizes=20,18,16,14,12
    </p>
    <p>
      ChapterStartOnEven=0
    </p>
    <p>
      NotesFontSize=8
    </p>
    <p>
      NotesInterline=1.5
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="pdf-small" ID="ID_685933298" CREATED="1539255531993" MODIFIED="1561643647418"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      ISBN=123-4-5678901-2-3
    </p>
    <p>
      PageWidth=100.5
    </p>
    <p>
      PageHeight=140
    </p>
    <p>
      TopMargin=15
    </p>
    <p>
      BottomMargin=8
    </p>
    <p>
      InnerMargin=15
    </p>
    <p>
      OuterMargin=10
    </p>
    <p>
      NormalFontSize=12
    </p>
    <p>
      Interlines=2
    </p>
    <p>
      ExtraInterPar=0.7
    </p>
    <p>
      TitleFontSizes=20,18,16
    </p>
    <p>
      ChapterStartOnEven=0
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="$Meta$" POSITION="left" ID="ID_1677497999" CREATED="1535807383941" MODIFIED="1535812809605">
<edge COLOR="#808080"/>
<node TEXT="Title" ID="ID_1846163087" CREATED="1535807399599" MODIFIED="1537308574400"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Mon super titre
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Author" ID="ID_765344758" CREATED="1535807401619" MODIFIED="1537308622714"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Jean-Yves Toumit
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Publisher" ID="ID_1075932294" CREATED="1538992490121" MODIFIED="1539255810905"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      JYTOU
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Language" ID="ID_478597071" CREATED="1535807495599" MODIFIED="1539257960372"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      EN
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Description" ID="ID_62556875" CREATED="1535807403995" MODIFIED="1537308634520"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Une superbe description
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="DateTZ" ID="ID_651011210" CREATED="1535807407914" MODIFIED="1537308657757"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      2018-09-19T12:33:22+11:44
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="UUID" ID="ID_268313362" CREATED="1535834111377" MODIFIED="1539255860361"><richcontent TYPE="NOTE" CONTENT-TYPE="xml/">
<html>
  <head>
    
  </head>
  <body>
    <p>
      12345678-90ab-cdef-0123-467890abcdef
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</map>
