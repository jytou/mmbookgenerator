package jyt.freemind.converter;

import junit.framework.TestCase;
import jyt.freemind.converter.epub.EpubBuilder;

public class EpubBuilderTestCase extends TestCase
{
	public void testRoman()
	{
		assertEquals("XXVI", EpubBuilder.toRoman(26, true));
		assertEquals("xix", EpubBuilder.toRoman(19, false));
		assertEquals("XVIII", EpubBuilder.toRoman(18, true));
		assertEquals("I", EpubBuilder.toRoman(1, true));
		assertEquals("II", EpubBuilder.toRoman(2, true));
		assertEquals("XIV", EpubBuilder.toRoman(14, true));
		assertEquals("XV", EpubBuilder.toRoman(15, true));
		assertEquals("XVI", EpubBuilder.toRoman(16, true));
		assertEquals("IX", EpubBuilder.toRoman(9, true));
	}
}
